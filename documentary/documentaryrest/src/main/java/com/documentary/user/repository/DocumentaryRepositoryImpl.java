package com.documentary.user.repository;

import static com.documentary.enums.UserRoleEnum.READER;
import static com.documentary.enums.UserRoleEnum.WRITER;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.request.DocumentaryRequestVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Documentary Repository Implementation"
 */
@Repository
public class DocumentaryRepositoryImpl implements CommonRepository<DocumentaryRequestVo, DocumentaryRequestVo> {
	
	/**
	 * @see "Pre-configured JdbcTemplate"
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#insert(java.lang.Object)
	 */
	@Override
	public int insert(DocumentaryRequestVo t) {
		String sql = "INSERT INTO documentary (title, description, user_id) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, t.instanceFactory());
	}

	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#update(java.lang.Object)
	 */
	@Override
	public int update(DocumentaryRequestVo t) {
		String sql = "UPDATE documentary SET title = ?, description = ? WHERE id = ?";
        return jdbcTemplate.update(sql, t.updateinstanceFactory());
	}

	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#delete(int)
	 */
	@Override
	public int delete(int id) {
		String sql = "DELETE FROM documentary where id = ?";
		return jdbcTemplate.update(sql, id);
	}

	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#getById(int, java.lang.String, java.lang.String)
	 */
	@Override
	public DocumentaryRequestVo getById(int id, String userId, String userRole) {
		StringBuilder sql = new  StringBuilder();
		sql.append("select distinct doc.id, doc.title, doc.description, concat(u.firstname , ' ' , u.lastname) as userId");
		sql.append(" from documentary doc");
		sql.append(" left outer join doc5_user u on (u.id = doc.user_id)");
		if(userRole.equalsIgnoreCase(WRITER.name())) {
			
			sql.append(" left outer join doc4_role ur on (ur.role_code = u.role_id)");
			sql.append(" where doc.id = ? and u.id = ? and ur.name = 'writer'");
			return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id, userId}, new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		}
		else if(userRole.equalsIgnoreCase(READER.name())) {
			sql.append(" where doc.id = ?");	
			return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id}, new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		} else {
			throw new EmptyResultDataAccessException(0);
		}
	}

	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#listAll(java.lang.String, java.lang.String)
	 */
	@Override
	public List<DocumentaryRequestVo> listAll(String userId, String userRole) {
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct doc.id, doc.title, doc.description");
		sql.append(" from documentary doc");
		if(userRole.equalsIgnoreCase(WRITER.name())) {
			sql.append(" left outer join doc5_user u on (u.id = doc.user_id)");
			sql.append(" left outer join doc4_role ur on (ur.role_code = u.role_id)");
			sql.append(" where u.id = ? and ur.name = 'writer'");
			sql.append(" order by doc.id desc");
			
	        return jdbcTemplate.query(sql.toString(),new Object[] {userId}, new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		}
		else if (userRole.equalsIgnoreCase(READER.name())) {
			sql.append(" order by doc.id desc");
	        return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		}
		else {
			throw new EmptyResultDataAccessException(0);
		}
	}

	/* (non-Javadoc)
	 * @see com.documentary.repository.CommonRepository#listLimited(java.lang.String, java.lang.String, int)
	 */
	@Override
	public List<DocumentaryRequestVo> listLimited(String userId, String userRole, int limit) {
		StringBuilder sql = new StringBuilder();
		sql.append("select distinct doc.id, doc.title, doc.description");
		sql.append(" from documentary doc");
		if(userRole.equalsIgnoreCase(WRITER.name())) {
			sql.append(" left outer join doc5_user u on (u.id = doc.user_id)");
			sql.append(" left outer join doc4_role ur on (ur.role_code = u.role_id)");
			sql.append(" where u.id = ? and ur.name = 'writer'");
			sql.append(" order by doc.id desc");
			sql.append(" limit " + limit);
			return jdbcTemplate.query(sql.toString(), new Object[] {userId}, new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		}
		else if (userRole.equalsIgnoreCase(READER.name())) {
			sql.append(" order by doc.id desc");
			sql.append(" limit " + limit);
			return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(DocumentaryRequestVo.class));
		}
		else {
			throw new EmptyResultDataAccessException(0);
		}
	}
}

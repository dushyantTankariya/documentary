package com.documentary.user.bo;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.helper.CommonHelper;
import com.documentary.user.repository.UserRepositoryImpl;
import com.documentary.vo.UserLoginVo;
import com.documentary.vo.request.UserLoginRequestVo;
import com.documentary.vo.request.UserRegisterRequestVo;
import com.documentary.vo.response.ResponseVo;
import com.documentary.vo.response.UserLoginResponseVo;
import com.util.auth.Authorization;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Service Layer extends common service methods"
 */
@Component
public class UserBoImpl extends AbstractBoImpl<UserRegisterRequestVo, UserLoginVo> {

	/**
	 * @see "UserRespository to authorize the user"
	 */
	@Autowired
	private UserRepositoryImpl repository;

	/**
	 * @see "Parameterized constructor to initialize repository in Generic Abstract Service."  
	 * */
	@Autowired
	public UserBoImpl(UserRepositoryImpl repository) {
		super(repository);
	}
	
	/**
	 * @see "To authorize the user"
	 * @param A Request in the form of {@link UserLoginRequestVo}
	 * @return A Response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	public ResponseVo login(UserLoginRequestVo request) throws Exception{
		UserLoginVo ur = repository.findByUser(request);
		return new CommonHelper().buildBaseDataResponse(new UserLoginResponseVo(ur, new Authorization().buildJwtToken(ur.getUsername(), ur.getPassword(),request.getRemember())));
	}
}

package com.documentary.user.controller;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.user.bo.DocumentaryBoImpl;
import com.documentary.vo.request.DocumentaryRequestVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest controller for Documentary"
 * */
@RestController
@RequestMapping("/documentary")
public class DocumentaryController {
	
	@Autowired
	private DocumentaryBoImpl service;
	
	/**
	 * @param A userId of user to validate user role
	 * @param a req in form of {@link DocumentaryRequestVo}
	 * @return A response in form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@PostMapping
	public ResponseVo insert(
			@RequestHeader(name="UserId") Integer userId,
			@RequestBody DocumentaryRequestVo req) throws Exception{
		if(service.documentaryHelper.isValidWriter(userId)) {
			return service.insert(req); 
		}else {
			throw new Exception(HttpStatus.UNAUTHORIZED.toString());
		}
	}
	
	/**
	 * @param A userId of user to validate user role
	 * @param A userRole to validate user access
	 * @param A req in the form of {@link DocumentaryRequestVo}
	 * @return A response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@PutMapping
	public ResponseVo update(
			@RequestHeader(name="UserId") Integer userId,
			@RequestHeader(name="UserRole") String userRole,
			@RequestBody DocumentaryRequestVo req) throws Exception{
		if(service.documentaryHelper.isValidWriterToUpdate(req.getId(), userId, userRole)) {
			return service.update(req);
		}else {
			throw new Exception(HttpStatus.UNAUTHORIZED.toString());
		}
	}
	
	/**
	 * @param A id to delete data
	 * @return A response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@DeleteMapping("/{id}")
	public ResponseVo delete(@PathVariable("id") Integer id) throws Exception{
		return service.delete(id);
	}
	
	/**
	 * @param A userId of user to validate user role
	 * @param A userRole to validate user access
	 * @param A id to find one row based on documentary Id 
	 * @return A response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/{id}")
	public ResponseVo getOne(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole,
			@PathVariable("id") Integer id
	) throws Exception{
		return service.getById(id, userId, userRole);
	}
	
	/**
	 * @param A userId of user to validate user role
	 * @param A userRole to validate user access
	 * @return A response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping
	public ResponseVo listAll(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole
	) throws Exception{
		return service.listAll(userId, userRole);
	}
	
	/**
	 * @param A userId of user to validate user role
	 * @param A userRole to validate user access
	 * @param A limit on list of records
	 * @return A response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/limit/{limit}")
	public ResponseVo listLimited(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole,
			@PathVariable("limit") int limit
	) throws Exception{
		return service.listLimited(userId, userRole, limit);
	}
	
}

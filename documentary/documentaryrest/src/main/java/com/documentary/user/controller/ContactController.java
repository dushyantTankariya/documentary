package com.documentary.user.controller;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.helper.CommonHelper;
import com.documentary.user.bo.ContactBoImpl;
import com.documentary.vo.ContactVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest controller for Contact"
 * */
@RestController
@RequestMapping("/contact")
public class ContactController {

	@Autowired
	private ContactBoImpl contactBoImpl;
	
	/**
	 * @param A req in the form of {@link ContactVo}
	 * @return A Success Response of contact
	 * @throws Any Exception
	 */
	@PostMapping("/")
	public ResponseVo insert(@RequestBody ContactVo req) throws Exception{
			return contactBoImpl.insert(req);
	}
	
	/**
	 * @see "To validate the token is alive"
	 * @return Response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/isTokenValid")
	public ResponseVo isTokenValid() throws Exception{
		return new CommonHelper().buildBaseResponse(1);
	}
}

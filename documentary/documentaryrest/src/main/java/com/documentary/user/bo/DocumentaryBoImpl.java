package com.documentary.user.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.helper.DocumentaryHelper;
import com.documentary.user.repository.DocumentaryRepositoryImpl;
import com.documentary.vo.request.DocumentaryRequestVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Documentary Service Implementation"
 *
 */
@Component
public class DocumentaryBoImpl extends AbstractBoImpl<DocumentaryRequestVo, DocumentaryRequestVo> {

	@Autowired
	public	DocumentaryHelper documentaryHelper;
	
	/**
	 * @see "Parameterized constructor to initialize repository in Generic Abstract Service."  
	 * */
	@Autowired
	public DocumentaryBoImpl(DocumentaryRepositoryImpl repository) {
		super(repository);
	}

}

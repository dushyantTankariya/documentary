/**
 * 
 */
package com.documentary.user.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.ContactVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Contact Repository Implementation"
 *
 */
@Repository
public class ContactRepositoryImpl implements CommonRepository<ContactVo, ContactVo> {
	/**
	 * @see "Pre-configured JdbcTemplate"
	 */
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insert(ContactVo t) throws Exception {
		String sql = "INSERT INTO sys_contact (name, email, phone, message) VALUES (?, ?, ?, ?)";
        return jdbcTemplate.update(sql, t.instanceFactory());
	}

	@Override
	public int update(ContactVo t) throws Exception {
		return 0;
	}

	@Override
	public int delete(int id) throws Exception {
		return 0;
	}

	@Override
	public ContactVo getById(int id, String userId, String userRole) throws Exception {
		return null;
	}

	@Override
	public List<ContactVo> listAll(String userId, String userRole) throws Exception {
		return null;
	}

	@Override
	public List<ContactVo> listLimited(String userId, String userRole, int limit) throws Exception {
		return null;
	}

}

package com.documentary.user.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.user.repository.ContactRepositoryImpl;
import com.documentary.vo.ContactVo;

@Service
public class ContactBoImpl extends AbstractBoImpl<ContactVo, ContactVo>{
	
	@Autowired
	ContactBoImpl(ContactRepositoryImpl contactRepositoryImpl) {
		super(contactRepositoryImpl);
	}
	
}

package com.documentary.user.controller;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.user.bo.UserBoImpl;
import com.documentary.vo.request.UserLoginRequestVo;
import com.documentary.vo.request.UserRegisterRequestVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest Controller for User"
 */
@RestController
@RequestMapping("/user")
public class UserController {
	
	/**
	 * @see "User service"
	 */
	@Autowired
	public UserBoImpl userBo;

	/**
	 * @see "Register a new user"
	 * @param a userRegister in the form of {@link UserRegisterRequestVo} 
	 * @return a response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@PostMapping("/register")
	public ResponseVo register(@RequestBody UserRegisterRequestVo userRegister) throws Exception{
		return userBo.insert(userRegister);
	}
	
	/**
	 * @see "Authorize a user"
	 * @param a userLogin in the form of {@link UserLoginRequestVo}
	 * @return a response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@PostMapping("/login")
	public ResponseVo login(@RequestBody UserLoginRequestVo userLogin) throws Exception{
		return userBo.login(userLogin);
	}
	
	/**
	 * @return a String Response "Application is running successfully!" to verify the API call is working as expected.
	 */
	@GetMapping("/test")
	public String test() {
		return "Application is running successfully!";
	}
	
}

package com.documentary.user.repository;

import static com.documentary.enums.ActiveDeleteEnum.ACTIVE;
import static com.documentary.enums.ActiveDeleteEnum.DEACTIVE;
import static com.documentary.enums.ActiveDeleteEnum.DELETE;
import static com.documentary.enums.ActiveDeleteEnum.AVAILABLE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.UserLoginVo;
import com.documentary.vo.request.UserLoginRequestVo;
import com.documentary.vo.request.UserRegisterRequestVo;
/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Repository Implementation"
 */
@Repository
public class UserRepositoryImpl implements CommonRepository<UserRegisterRequestVo, UserLoginVo> {
	 
	@Autowired
	private JdbcTemplate jdbcTemplate;
	 
		/**
	 * @return int rows affected
	 * @param {@link UserRegisterRequestVo}
	 * @see com.documentary.common.repository.CommonRepository#insert(java.lang.Object)
	 */
	@Override
	public int insert(UserRegisterRequestVo t) throws Exception{
		String sql = "INSERT INTO doc5_user (firstname, lastname, username, password, email, role_id) VALUES (?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, t.instanceFactory());
		
	}

	/**
	 * @return int rows affected
	 * @param {@link UserRegisterRequestVo}
	 * @see com.documentary.common.repository.CommonRepository#insert(java.lang.Object)
	 */
	@Override
	public int update(UserRegisterRequestVo t) throws Exception{
		String sql = "UPDATE doc5_user SET firstname = ?, lastname = ?, username = ?, password = ?, role_id = ? WHERE id = ?";
        return jdbcTemplate.update(sql, t.updateinstanceFactory());
	}
	
	/**
	 * @return int rows affected
	 * @param the id
	 * @see com.documentary.common.repository.CommonRepository#insert(java.lang.Object)
	 */
	@Override
	public int delete(int id) throws Exception{
		String sql = "UPDATE FROM doc5_user SET is_active = ?, is_delete = ? where id = ?";
		return jdbcTemplate.update(sql, new Object[] {DEACTIVE.value, DELETE.value, id});
	}

	/**
	 * @return the UserResponse
	 * @param the id
	 * @see com.documentary.common.repository.CommonRepository#getById(java.lang.Object)
	 */
	@Override
	public UserLoginVo getById(int id,  String userId, String userRole) throws Exception{
		String sql = "select distinct u.id, u.firstname, u.lastname, u.username, u.password, u.email, ur.name as 'role'"
				+ " from doc5_user u"
				+ " left outer join doc4_role ur on (ur.role_code = u.role_id)"
				+ " where u.id = ? and u.is_active = ? and u.is_delete = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {id, ACTIVE.value, AVAILABLE.value}, new BeanPropertyRowMapper<UserLoginVo>(UserLoginVo.class));
	}

	public UserLoginVo findByUser(UserLoginRequestVo request) throws Exception{
		UserLoginVo userLoginVo;
		String sql = "select distinct u.id, u.firstname, u.lastname, u.username, u.password, u.email, ur.name as 'role'"
				+ " from doc5_user u"
				+ " left outer join doc4_role ur on (ur.role_code = u.role_id)"
				+ " where username = ? and password = ? and u.is_active = ? and u.is_delete = ?";
		try {
			userLoginVo = jdbcTemplate.queryForObject(sql, request.findByInstanceFactory(), new BeanPropertyRowMapper<UserLoginVo>(UserLoginVo.class));
			
			//Log the audit
	        jdbcTemplate.update("INSERT INTO sys_audit (username, state) VALUES (?, ?)", new Object[] {userLoginVo.getUsername(),"AUTHENTICATION_SUCCESS"});
		}catch(EmptyResultDataAccessException e) {
			//Log the audit
			jdbcTemplate.update("INSERT INTO sys_audit (username, state) VALUES (?, ?)", new Object[] {request.getUsername(),"AUTHENTICATION_FAILED"});
			throw e;
		}
		return userLoginVo;
	}
	
	/**
	 * @return the list of {@link UserLoginVo}
	 * @see com.documentary.common.repository.CommonRepository#listAll(java.lang.Object)
	 */
	@Override
	public List<UserLoginVo> listAll(String userId, String userRole) throws Exception{
		String sql = "select distinct u.id, u.firstname, u.lastname, u.username, u.password, u.email, ur.name as 'role'"
				+ " from doc5_user u"
				+ " left outer join doc4_role ur on (ur.role_code = u.role_id)"
				+ " where u.is_active = ? and u.is_delete = ?";
        return jdbcTemplate.query(sql, new Object[] {ACTIVE.value, AVAILABLE.value}, new BeanPropertyRowMapper<>(UserLoginVo.class));
	}	
	
	/**
	 * @return the list of {@link UserLoginVo}
	 * @see com.documentary.repository.CommonRepository#listLimited(int)(java.lang.Object)
	 */
	@Override
	public List<UserLoginVo> listLimited(String userId, String userRole, int limit) throws Exception{
		String sql = "select distinct u.id, u.firstname, u.lastname, u.username, u.password, u.email, ur.name as 'role'"
				+ " from doc5_user u"
				+ " left outer join doc4_role ur on (ur.role_code = u.role_id)"
				+ " where u.is_active = ? and u.is_delete = ?"
				+ " limit " + limit;
        return jdbcTemplate.query(sql, new Object[] {ACTIVE.value, AVAILABLE.value}, new BeanPropertyRowMapper<>(UserLoginVo.class));
	}	
}

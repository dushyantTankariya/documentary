package com.documentary.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Application configuration goes here ({@link EnableWebMvc} & {@link ComponentScan})"
 */
@EnableWebMvc
@EnableAspectJAutoProxy(proxyTargetClass = true)
@ComponentScan(basePackages = "com.documentary")
@PropertySource(value = {"classpath:datasource.properties"})
public class ApplicationConfiguration {
	
	/**
	 * @see "Environment to read values from files"
	 */
	@Autowired
	private Environment env;

	/**
	 * @return dataSource - configured using DrivermanagerDataSource
	 * @see "Configuration from datasource.properties file"
	 */
	@Bean
	DataSource dataSource() {
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getRequiredProperty("jdbc.driverClassName"));
		dataSource.setUrl(env.getRequiredProperty("jdbc.url"));
		dataSource.setUsername(env.getRequiredProperty("jdbc.username"));
		return dataSource;
	}

	/**
	 * @param dataSource configured
	 * @return jdbcTemplate ready with implemented data source
	 * @see "configure jdbcTemplate for transaction managment"
	 */
	@Bean
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		return jdbcTemplate;
	}
}

package com.documentary.config;

import javax.management.RuntimeErrorException;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.documentary.vo.response.ResponseVo;
import com.documentary.vo.response.impl.BaseResponseImplVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Global Exception Handler"
 */
@ControllerAdvice(basePackages = "com.documentary")
public class ApplicationErrorHandler {

	/**
	 * @see "Any exception"
	 * @param e as any Exception
	 * @return A Response in the form of ResponseEntity 
	 */
	@ExceptionHandler(Exception.class)
	public ResponseEntity < ResponseVo > exception(final Exception e) {
		BaseResponseImplVo baseResponseImplVo;
		HttpStatus httpStatus;
		if(e instanceof DuplicateKeyException) {
			baseResponseImplVo = new BaseResponseImplVo(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST);
			httpStatus = HttpStatus.BAD_REQUEST;
			
		} else if(e instanceof EmptyResultDataAccessException || e instanceof RuntimeException) {
			baseResponseImplVo = new BaseResponseImplVo(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND);
			httpStatus = HttpStatus.NOT_FOUND;
			
		} else if (e.getMessage().equalsIgnoreCase(HttpStatus.UNAUTHORIZED.toString())) {
			baseResponseImplVo = new BaseResponseImplVo(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED);
			httpStatus = HttpStatus.UNAUTHORIZED;
		}else if(e instanceof RuntimeErrorException) {
			baseResponseImplVo = new BaseResponseImplVo(HttpStatus.INTERNAL_SERVER_ERROR.value(), HttpStatus.INTERNAL_SERVER_ERROR);
			httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
		}else {
			baseResponseImplVo = new BaseResponseImplVo(HttpStatus.UNPROCESSABLE_ENTITY.value(), HttpStatus.UNPROCESSABLE_ENTITY);
			httpStatus = HttpStatus.UNPROCESSABLE_ENTITY;
		}
		return new ResponseEntity < > (baseResponseImplVo, httpStatus);		
	}
}

package com.documentary.ops.controller;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.helper.CommonHelper;
import com.documentary.ops.bo.HealthBo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Health checkup of the system includes Database, Diskspace, and Application"
 */
@RestController
@RequestMapping("/health")
public class HealthController {

	@Autowired
	HealthBo healthBo;
	
	@Autowired
	CommonHelper commonHelper;
	
	/**
	 * @return A Boolean value of is Database Alive Response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/db")
	public ResponseVo isDatabaseAlive() throws Exception{
		return healthBo.isDbAlive();
	}
	
	/**
	 * @return A diskspace value in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/diskSpace")
	public ResponseVo diskSpace() throws Exception{		
		return healthBo.calculateDiskSpace();
	}
	
	/**
	 * @return A Boolean value of is application alive in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping("/application")
	public ResponseVo isApplicationAlive() throws Exception {
		return commonHelper.buildBaseResponse(1);
	}
	
	
	
}

/**
 * 
 */
package com.documentary.ops.bo;

import static com.documentary.helper.DashboardHelper.calculateDiskSpace;
import static com.documentary.helper.DashboardHelper.calculateHeapMemory;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;

import org.springframework.stereotype.Service;

import com.documentary.vo.DiskSpaceAndMemoryVo;
import com.documentary.vo.ProcessorVo;
import com.documentary.vo.ThreadVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Dashboard Bussiness Object/Service"
 */
@Service
public class DashboardBo {
	
	/**
	 * @see "Jvm Metrics details such as total, max, and free memory"
	 * @return a Response in the form of {@link ResponseVo}
	 */
	public DiskSpaceAndMemoryVo diskSpaceInfo() throws Exception{
		DiskSpaceAndMemoryVo o = new DiskSpaceAndMemoryVo();
		o.setDiskSpace(calculateDiskSpace());
		o.setHeap(calculateHeapMemory());
    	return o;
	}
	
	/**
	 * @see "Thread Information"
	 * @return a response in the form of {@link ResponseVo}
	 */
	public ThreadVo threadInfo() throws Exception{
		ThreadVo o = new ThreadVo();
		ThreadMXBean t= ManagementFactory.getThreadMXBean();
		o.setActiveCount(Thread.activeCount());	
		o.setDaemonCount(t.getDaemonThreadCount());
		o.setTotalStartedCount(t.getTotalStartedThreadCount());
		return o;
	}
	
	/**
	 * @see "Process Information"
	 * @return a response in the form of {@link ResponseVo}
	 */
	public ProcessorVo systemInfo() throws Exception{
		ProcessorVo o = new ProcessorVo();
		OperatingSystemMXBean s =ManagementFactory.getOperatingSystemMXBean();
		RuntimeMXBean r =ManagementFactory.getRuntimeMXBean();
		
		o.setOsName(s.getName());
		o.setOsVersion(s.getVersion());
		o.setLoadAvg(s.getSystemLoadAverage());
		o.setAvailableProcess(s.getAvailableProcessors());
		
		o.setUpTime(r.getUptime());
		o.setStartTime(r.getStartTime());
		return o;
	}
	
}

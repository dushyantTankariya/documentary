package com.documentary.ops.bo;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.documentary.helper.CommonHelper;
import com.documentary.ops.repository.HealthRepository;
import com.documentary.vo.HealthDatabaseVo;
import com.documentary.vo.HealthDiskSpaceVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Health Business Object/Service"
 */
@Service
public class HealthBo {
	
	@Autowired
	HealthRepository healthRepository;
	
	@Autowired
	CommonHelper commonHelper;
	
	/**
	 * @return a flag value true or false in form of Boolean
	 */
	@Transactional
	public ResponseVo isDbAlive() throws Exception{
		HealthDatabaseVo o = healthRepository.isDbAlive();
		return commonHelper.buildBaseDataResponse(o);
	}
	
	
	/**
	 * @return A Disk Space long value
	 */
	public ResponseVo calculateDiskSpace() throws Exception{
		HealthDiskSpaceVo healthDiskSpaceVo = new HealthDiskSpaceVo();
		File file = new File("/");
		double free = file.getFreeSpace() / (1024.0 * 1024 * 1024);
		double total =  file.getTotalSpace() / (1024.0 * 1024 * 1024);
		double used = (total - free);		
		
		healthDiskSpaceVo.setFree(free);
		healthDiskSpaceVo.setTotal(total);
		healthDiskSpaceVo.setThreshold(used);
		
		return commonHelper.buildBaseDataResponse(healthDiskSpaceVo);
	}
}

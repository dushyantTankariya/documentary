package com.documentary.ops.controller;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.ops.bo.LoggerBo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Application Log changes goes here"
 */
@RestController
@RequestMapping("/logger")
public class LogsController {

	@Autowired
	LoggerBo loggerBo;

	/**
	 * @return A initial logger details in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@GetMapping
	public ResponseVo getInitloggerDetails() throws Exception {
		return loggerBo.getInit();
	}

	/**
	 * @param A loglevel to set on class
	 * @param A clazz name to set log level
	 * @return A success response in the form of {@link ResponseVo}
	 * @throws Any Exception
	 */
	@PostMapping("/{logLevel}")
	public ResponseVo setLogLevel(@RequestBody String clazz, @PathVariable String logLevel) throws Exception {
		return loggerBo.setLogLevel(clazz, logLevel);
	}

}

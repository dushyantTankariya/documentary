package com.documentary.ops.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.vo.HealthDatabaseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Health Repository"
 */
@Repository
public class HealthRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/**
	 * @return a flag value true or false based on db connection
	 * @throws Exception
	 */
	public HealthDatabaseVo isDbAlive() throws Exception{
		String sql = "SELECT 1";
		Integer result = jdbcTemplate.queryForObject(sql, Integer.class);
		HealthDatabaseVo healthDatabaseVo = new HealthDatabaseVo();
		healthDatabaseVo.setDatabase(jdbcTemplate.getDataSource().getConnection().getMetaData().getDatabaseProductName());
		healthDatabaseVo.setResult(result);
		healthDatabaseVo.setValidationQuery(sql);
		return healthDatabaseVo;
	}

}

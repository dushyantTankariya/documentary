package com.documentary.ops.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.helper.CommonHelper;
import com.documentary.ops.bo.DashboardBo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Dashboard controller"
 */
@RestController
@RequestMapping("/dashboard")
public class DashboardController {

	@Autowired
	private DashboardBo bo;
	
	@Autowired
	public CommonHelper commonHelper;
	
	/**
	 * @return A response in the form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@GetMapping("/")
	public ResponseVo data() throws Exception{
		Object o = new Object[] {
				bo.diskSpaceInfo(),
				bo.threadInfo(),
				bo.systemInfo()
		};
		 
		return commonHelper.buildBaseDataResponse(o);
		
	}
}

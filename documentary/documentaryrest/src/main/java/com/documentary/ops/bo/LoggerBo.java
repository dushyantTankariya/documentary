package com.documentary.ops.bo;

import java.util.LinkedHashMap;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.helper.CommonHelper;
import com.documentary.vo.response.ResponseVo;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.LoggerContext;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Loggeer Businees Object"
 */
@Service
public class LoggerBo {

	
	@Autowired
	public CommonHelper commonHelper;
	
	public ResponseVo getInit() {
		LinkedHashMap<String, String> o = new LinkedHashMap<>();
		ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
		
		scanner.addIncludeFilter(new AnnotationTypeFilter(RestController.class));
		scanner.addIncludeFilter(new AnnotationTypeFilter(Service.class));
		scanner.addIncludeFilter(new AnnotationTypeFilter(Repository.class));
		scanner.findCandidateComponents("com.documentary")
				.forEach(e -> o.put(e.getBeanClassName(),						
						((ch.qos.logback.classic.Logger)( (LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(e.getBeanClassName())).getLevel() !=null
						? ((ch.qos.logback.classic.Logger)( (LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(e.getBeanClassName())).getLevel().toString()
						: Level.DEBUG.toString()));
		return commonHelper.buildBaseDataResponse(o);
	}
	
	public ResponseVo setLogLevel(String packageName, String logLevel) {
		
		packageName = packageName.substring(13,packageName.length());
		packageName = packageName.replace("\"", "").replace("{", "").replace("}", "");
		
		((ch.qos.logback.classic.Logger)((LoggerContext) LoggerFactory.getILoggerFactory()).getLogger(packageName)).setLevel(Level.toLevel(logLevel));
		
		return commonHelper.buildBaseResponse(1);
	}
}

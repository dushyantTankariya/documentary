package com.documentary.enums;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "ActiveDeleteEnum for Record to mark as Active, Deactive, Delete, and Available."
 */
public enum ActiveDeleteEnum {
	ACTIVE(1),
	DEACTIVE(0),
	DELETE(1),
	AVAILABLE(0);
	
	/**
	 * @see "to read value from enum"
	 */
	public int value;

	/**
	 * @param A selected enum value
	 */
	ActiveDeleteEnum(int value) {
		this.value = value;
	}
}

package com.documentary.enums;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "AuthEnum for security"
 */
public enum AuthEnum {
	SHA_KEY("MTIzNDU2Nzg=");

	/**
	 * @see "to read value from enum"
	 */
	public String value;

	/**
	 * @param A selected enum value
	 */
	AuthEnum(String value) {
		this.value = value;
	}
}

package com.documentary.enums;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Role Enums"
 */
public enum UserRoleEnum {
	WRITER, READER
}

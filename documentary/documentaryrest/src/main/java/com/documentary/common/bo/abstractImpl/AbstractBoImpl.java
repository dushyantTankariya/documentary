/**
 * 
 */
package com.documentary.common.bo.abstractImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.documentary.common.bo.CommonBo;
import com.documentary.common.repository.CommonRepository;
import com.documentary.helper.CommonHelper;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @param <T>
 * @see "An implementation of common Service methods."
 */
@Service
@Transactional
public abstract class AbstractBoImpl<T,O> implements CommonBo<T> {

	@Autowired
	private CommonHelper commonHelper;
	
	private CommonRepository<T, O> repository;
		
	public AbstractBoImpl(CommonRepository<T, O> repository) {
		this.repository = repository;
	}
/*
	public void setRepository(CommonRepository<T, O> repository) {
		this.repository = repository;
	}*/
	
	@Override
	public ResponseVo insert(T t) throws Exception{
		return commonHelper.buildBaseResponse(repository.insert(t));
	}

	@Override
	public ResponseVo update(T t) throws Exception{
		return commonHelper.buildBaseResponse(repository.update(t));
	}
	
	@Override
	public ResponseVo delete(int id) throws Exception{
		return commonHelper.buildBaseResponse(repository.delete(id));
	}

	@Override
	public ResponseVo getById(int id, String userId, String userRole) throws Exception{
		return commonHelper.buildBaseDataResponse(repository.getById(id, userId, userRole));
	}

	@Override
	public ResponseVo listAll(String userId, String userRole) throws Exception{
		return commonHelper.buildBaseDataResponse(repository.listAll(userId, userRole));
	}
	
	@Override
	public ResponseVo listLimited(String userId, String userRole, int limit) throws Exception{
		return commonHelper.buildBaseDataResponse(repository.listLimited(userId, userRole, limit));
	}
}

package com.documentary.common.bo;

import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @param <T>
 * @see "Common Generic Service Layer interface"
 */
public interface CommonBo<T> {
	public ResponseVo insert(T t) throws Exception;
	public ResponseVo update(T t) throws Exception;
	public ResponseVo delete(int id) throws Exception;
	public ResponseVo getById(int id, String userId, String userRole) throws Exception;
	public ResponseVo listAll(String userId, String userRole) throws Exception;
	public ResponseVo listLimited(String userId, String userRole, int limit) throws Exception;

}

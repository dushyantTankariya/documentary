package com.documentary.common.repository;

import java.util.List;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @param <T>
 * @see "Common Generic Service Layer interface"
 */
public interface CommonRepository<T,O> {
	
	public int insert(T t) throws Exception;
	public int update(T t) throws Exception;
	public int delete(int id) throws Exception;
	public O getById(int id, String userId, String userRole) throws Exception;
	public List<O> listAll(String userId, String userRole) throws Exception;
	public List<O> listLimited(String userId, String userRole, int limit) throws Exception;

}

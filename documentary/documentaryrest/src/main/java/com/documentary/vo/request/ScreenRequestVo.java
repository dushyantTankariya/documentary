package com.documentary.vo.request;

public class ScreenRequestVo {

	private String id;
	private String name;
	private String version;
	private String submoduleId;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}
	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}
	/**
	 * @return the submoduleId
	 */
	public String getSubmoduleId() {
		return submoduleId;
	}
	/**
	 * @param submoduleId the submoduleId to set
	 */
	public void setSubmoduleId(String submoduleId) {
		this.submoduleId = submoduleId;
	}

}

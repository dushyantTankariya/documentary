package com.documentary.vo.request;

import org.springframework.stereotype.Component;

import static com.documentary.enums.ActiveDeleteEnum.ACTIVE;
import static com.documentary.enums.ActiveDeleteEnum.AVAILABLE;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Login Request Value Object/Model"
 */
@Component
public class UserLoginRequestVo {

	private String username;
	private String password;
	private String remember;
	
	public Object[] instanceFactory() {
		return new Object[] {username, password};
	}
	
	public Object[] findByInstanceFactory() {
		return new Object[] {username, password, ACTIVE.value, AVAILABLE.value};
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	
	/**
	 * @return the remember
	 */
	public String getRemember() {
		return remember;
	}

	/**
	 * @param remember the remember to set
	 */
	public void setRemember(String remember) {
		this.remember = remember;
	}
}

package com.documentary.vo.response.impl;

import org.springframework.http.HttpStatus;

import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Base Data Response"
 */
public class BaseDataResponseImplVo implements ResponseVo{

	private Integer status;
	private HttpStatus message;
	private Object data;
	
	

	/**
	 * @param status
	 * @param message
	 * @param data
	 */
	public BaseDataResponseImplVo(Integer status, HttpStatus message, Object data) {
		super();
		this.status = status;
		this.message = message;
		this.data = data;
	}

	/**
	 * @return the status
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the message
	 */
	public HttpStatus getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(HttpStatus message) {
		this.message = message;
	}

	
	/**
	 * @return the data
	 */
	public Object getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Object data) {
		this.data = data;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "BaseDataResponseImplVo [status=" + status + ", message=" + message + ", data=" + data + "]";
	}
	
	
}

package com.documentary.vo;

import org.springframework.stereotype.Component;

import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Process as Response"
 */
@Component
public class ProcessorVo implements ResponseVo{

	private String osName;
	private String osVersion;
	private double loadAvg;
	private int availableProcess;
	private long upTime;
	private long startTime;
	
	/**
	 * @return the osName
	 */
	public String getOsName() {
		return osName;
	}

	/**
	 * @param osName the osName to set
	 */
	public void setOsName(String osName) {
		this.osName = osName;
	}

	/**
	 * @return the osVersion
	 */
	public String getOsVersion() {
		return osVersion;
	}

	/**
	 * @param osVersion the osVersion to set
	 */
	public void setOsVersion(String osVersion) {
		this.osVersion = osVersion;
	}

	/**
	 * @return the loadAvg
	 */
	public double getLoadAvg() {
		return loadAvg;
	}

	/**
	 * @param loadAvg the loadAvg to set
	 */
	public void setLoadAvg(double loadAvg) {
		this.loadAvg = loadAvg;
	}

	/**
	 * @return the availableProcess
	 */
	public int getAvailableProcess() {
		return availableProcess;
	}

	/**
	 * @param availableProcess the availableProcess to set
	 */
	public void setAvailableProcess(int availableProcess) {
		this.availableProcess = availableProcess;
	}

	/**
	 * @return the upTime
	 */
	public long getUpTime() {
		return upTime;
	}

	/**
	 * @param upTime the upTime to set
	 */
	public void setUpTime(long upTime) {
		this.upTime = upTime;
	}

	/**
	 * @return the startTime
	 */
	public long getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

}

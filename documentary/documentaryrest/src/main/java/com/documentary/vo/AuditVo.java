package com.documentary.vo;

import java.sql.Timestamp;

public class AuditVo {

	private Integer id;
	private String username;
	private String state;
	private Timestamp statetimestamp;
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * @return the statetimestamp
	 */
	public Timestamp getStatetimestamp() {
		return statetimestamp;
	}
	/**
	 * @param statetimestamp the statetimestamp to set
	 */
	public void setStatetimestamp(Timestamp statetimestamp) {
		this.statetimestamp = statetimestamp;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuditVo [id=" + id + ", username=" + username + ", state=" + state + ", statetimestamp="
				+ statetimestamp + "]";
	}
}

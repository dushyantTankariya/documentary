package com.documentary.vo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Health Disk Space Value Object/ POJO/ MODEL"
 */
public class HealthDiskSpaceVo {

	private double total;
	private double free;
	private double threshold;
	
	/**
	 * @return the total
	 */
	public double getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(double total) {
		this.total = total;
	}

	/**
	 * @return the free
	 */
	public double getFree() {
		return free;
	}

	/**
	 * @param free the free to set
	 */
	public void setFree(double free) {
		this.free = free;
	}

	/**
	 * @return the threshold
	 */
	public double getThreshold() {
		return threshold;
	}

	/**
	 * @param threshold the threshold to set
	 */
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}

}

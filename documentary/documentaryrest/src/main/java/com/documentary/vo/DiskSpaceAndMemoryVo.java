/**
 * 
 */
package com.documentary.vo;

import org.springframework.stereotype.Component;

import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see  "Jvm Merics Importation as Response"
 */
@Component
public class DiskSpaceAndMemoryVo implements ResponseVo{

	private Long diskSpace;
	private Long heap;
	
	/**
	 * @return the runtime
	 */
	public Long getDiskSpace() {
		return diskSpace;
	}

	/**
	 * @param runtime the runtime to set
	 */
	public void setDiskSpace(Long diskSpace) {
		this.diskSpace = diskSpace;
	}

	/**
	 * @return the heap
	 */
	public Long getHeap() {
		return heap;
	}

	/**
	 * @param heap the heap to set
	 */
	public void setHeap(Long heap) {
		this.heap = heap;
	}
}

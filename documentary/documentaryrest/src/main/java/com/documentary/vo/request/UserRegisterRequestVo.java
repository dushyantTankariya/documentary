package com.documentary.vo.request;

import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Registration Request"
 */
@Component
public class UserRegisterRequestVo {

	private Integer id;
	private String firstname;
	private String lastname;
	private String username;
	private String password;
	private String email;
	private Integer roleId;


	public Object[] instanceFactory() {
		return new Object[] {firstname, lastname, username, password, email, roleId};
	}
	
	public Object[] updateinstanceFactory() {
		return new Object[] {firstname, lastname, username, password, email, roleId, id};
	}
	
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return firstname of user registration
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname of user registration
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return lastname of user registration
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname of user registration
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return username of user registration
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username of user registration
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return password of user registration
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password of user registration
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return email of user registration
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email of user registration
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	
	/**
	 * @return the roleId
	 */
	public Integer getRoleId() {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}

}

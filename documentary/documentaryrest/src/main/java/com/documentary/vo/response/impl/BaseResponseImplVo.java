package com.documentary.vo.response.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;

import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Base Response"
 */
public class BaseResponseImplVo implements ResponseVo {
	
	private Integer status;
	private HttpStatus message;

	
	/**
	 * @param status
	 * @param message
	 */
	@Autowired(required=false)
	public BaseResponseImplVo(Integer status, HttpStatus message) {
		super();
		this.status = status;
		this.message = message;
	}

	/**
	 * @return the status for HTTP response
	 */
	public Integer getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Integer status) {
		this.status = status;
	}

	/**
	 * @return the message for HTTP response
	 */
	public HttpStatus getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(HttpStatus message) {
		this.message = message;
	}

}

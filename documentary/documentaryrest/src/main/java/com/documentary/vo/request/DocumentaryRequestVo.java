package com.documentary.vo.request;

import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Documentary Request Value Object/Model"
 */
@Component
public class DocumentaryRequestVo {

	private Integer id;
	private String title;
	private String description;
	private String userId;
	
	public Object[] instanceFactory() {
		return new Object[] {title, description, userId};
	}

	public Object[] updateinstanceFactory() {
		return new Object[] {title, description, id};
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

}

package com.documentary.vo.response;

import com.documentary.vo.UserLoginVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Login Response Value Object/Model"
 */
public class UserLoginResponseVo {
	
	private Integer id;
	private String firstname;
	private String lastname;
	private String email;
	private String role;
	private String token;
	
	public UserLoginResponseVo(UserLoginVo login, String token) {
		this.id = login.getId();
		this.firstname = login.getFirstname();
		this.lastname = login.getLastname();
		this.email = login.getEmail();
		this.role=login.getRole();
		this.token = token;
	}

	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the firstname
	 */
	public String getFirstname() {
		return firstname;
	}

	/**
	 * @param firstname the firstname to set
	 */
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	/**
	 * @return the lastname
	 */
	public String getLastname() {
		return lastname;
	}

	/**
	 * @param lastname the lastname to set
	 */
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

}

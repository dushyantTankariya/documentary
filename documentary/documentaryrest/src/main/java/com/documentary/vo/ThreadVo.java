package com.documentary.vo;

import org.springframework.stereotype.Component;

import com.documentary.vo.response.ResponseVo;

@Component
public class ThreadVo implements ResponseVo {

	private Integer activeCount;
	private Integer daemonCount;
	private Long totalStartedCount;

	/**
	 * @return the activeCount
	 */
	public Integer getActiveCount() {
		return activeCount;
	}

	/**
	 * @param activeCount the activeCount to set
	 */
	public void setActiveCount(Integer activeCount) {
		this.activeCount = activeCount;
	}

	/**
	 * @return the blockCount
	 */
	public Integer getDaemonCount() {
		return daemonCount;
	}

	/**
	 * @param daemonCount the daemonCount to set
	 */
	public void setDaemonCount(Integer daemonCount) {
		this.daemonCount = daemonCount;
	}

	/**
	 * @return the totalStartedCount
	 */
	public Long getTotalStartedCount() {
		return totalStartedCount;
	}

	/**
	 * @param totalStartedCount the totalStartedCount to set
	 */
	public void setTotalStartedCount(Long totalStartedCount) {
		this.totalStartedCount = totalStartedCount;
	}

}

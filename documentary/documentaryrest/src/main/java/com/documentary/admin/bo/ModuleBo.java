package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.ModuleRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.request.ModuleRequestVo;
import com.documentary.vo.response.ModuleResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Module Management Service Implementation"
 *
 */
@Component
public class ModuleBo  extends AbstractBoImpl<ModuleRequestVo, ModuleResponseVo>  {

	@Autowired
	public ModuleBo(ModuleRepository moduleRepository) {
		super(moduleRepository);
	}

}

package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.UserRoleRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.UserRoleVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Role Service Implementation"
 *
 */
@Component
public class UserRoleBo extends AbstractBoImpl<UserRoleVo, UserRoleVo>{
	
	@Autowired
	public UserRoleBo(UserRoleRepository repository) {
		super(repository);
	}

}

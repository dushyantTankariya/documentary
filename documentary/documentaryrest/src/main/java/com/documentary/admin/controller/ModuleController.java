package com.documentary.admin.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.admin.bo.ModuleBo;
import com.documentary.vo.request.ModuleRequestVo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Rest controller for Site Module Management"
 * */
@RestController
@RequestMapping("/module")
public class ModuleController {
	
	@Autowired
	private ModuleBo service;
	
	public static int userId;

	/**
	 * @param A req
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@PostMapping
	public ResponseVo insert(
			@RequestHeader(name="UserId") String userId,
			@RequestBody ModuleRequestVo req) throws Exception {
		ModuleController.userId = Integer.parseInt(userId);
		return service.insert(req);
	}
	
	/**
	 * @param req
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@PutMapping
	public ResponseVo update(
			@RequestHeader(name="UserId") String userId,
			@RequestBody ModuleRequestVo req) throws Exception{
		ModuleController.userId = Integer.parseInt(userId);
		return service.update(req);
	}
	
	/**
	 * @param A user id
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@DeleteMapping("/{id}")
	public ResponseVo delete(@PathVariable("id") Integer id) throws Exception{
		return service.delete(id);
	}
	
	/**
	 * @param A userId
	 * @param A userRole
	 * @param A User id to get One
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@GetMapping("/{id}")
	public ResponseVo getOne(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole,
			@PathVariable("id") Integer id
	) throws Exception{
		return service.getById(id, userId, userRole);
	}
	
	/**
	 * @param A userId
	 * @param A userRole
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@GetMapping
	public ResponseVo listAll(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole
	) throws Exception{
		return service.listAll(userId, userRole);
	}
	
	/**
	 * @param A userId
	 * @param A userRole
	 * @param A limit
	 * @return Response in form of {@link ResponseVo}
	 * @throws A Exception
	 */
	@GetMapping("/limit/{limit}")
	public ResponseVo listLimited(
			@RequestHeader(name="UserId") String userId,
			@RequestHeader(name="UserRole") String userRole,
			@PathVariable("limit") int limit
	) throws Exception{
		return service.listLimited(userId, userRole, limit);
	}

}

package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.UserMgmtRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.UserLoginVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Management Service Implementation"
 *
 */
@Component
public class UserMgmtBo extends AbstractBoImpl<UserLoginVo, UserLoginVo> {
	
	@Autowired
	public UserMgmtBo(UserMgmtRepository repository) {
		super(repository);
	}

}

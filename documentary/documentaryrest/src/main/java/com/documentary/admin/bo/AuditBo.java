package com.documentary.admin.bo;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.documentary.admin.repository.AuditRepository;
import com.documentary.helper.CommonHelper;
import com.documentary.vo.AuditVo;
import com.documentary.vo.response.ResponseVo;

@Service
public class AuditBo {
	
	@Autowired
	AuditRepository auditRepository;
	
	@Autowired
	CommonHelper commonHelper;
	
	@Transactional
	public ResponseVo listAudit(Timestamp fromTimestamp, Timestamp toTimestamp) throws Exception{
		List<AuditVo> o = auditRepository.listAudit(fromTimestamp, toTimestamp);
		return commonHelper.buildBaseDataResponse(o);
	}
}

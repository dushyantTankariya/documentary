package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.SubModuleRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.request.SubModuleRequestVo;
import com.documentary.vo.response.SubModuleResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Sub Module Management Service Implementation"
 *
 */
@Component
public class SubModuleBo extends AbstractBoImpl<SubModuleRequestVo, SubModuleResponseVo>  {

	@Autowired
	public SubModuleBo(SubModuleRepository subModuleRepository) {
		super(subModuleRepository);
	}

}

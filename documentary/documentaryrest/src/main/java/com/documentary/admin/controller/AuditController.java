package com.documentary.admin.controller;

import java.sql.Timestamp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.documentary.admin.bo.AuditBo;
import com.documentary.vo.response.ResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Audit controller"
 */
@RestController
@RequestMapping("/audit")
public class AuditController {
	
	@Autowired
	private AuditBo auditBo;
	
	@GetMapping("/{fromTimestamp}/{toTimestamp}")
	public ResponseVo listAll(@PathVariable Timestamp fromTimestamp, @PathVariable Timestamp toTimestamp) throws Exception{
		return auditBo.listAudit(fromTimestamp, toTimestamp);
	} 

}

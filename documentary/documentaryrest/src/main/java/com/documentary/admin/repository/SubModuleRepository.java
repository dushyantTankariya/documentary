package com.documentary.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.admin.controller.SubModuleController;
import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.request.SubModuleRequestVo;
import com.documentary.vo.response.SubModuleResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Sub Module Management Repository"
 */
@Repository
public class SubModuleRepository implements CommonRepository<SubModuleRequestVo, SubModuleResponseVo>{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insert(SubModuleRequestVo t) throws Exception {
		String sql = "INSERT INTO doc2_sub_module (name, module_id, created_by, created_date) VALUES (?, ?, ?, SYSDATE())";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), t.getModuleId(), SubModuleController.userId });
	}

	@Override
	public int update(SubModuleRequestVo t) throws Exception {
		String sql = "UPDATE doc2_sub_module SET name = ?, module_id = ?, updated_by = ?, updated_date = SYSDATE() WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), t.getModuleId(), SubModuleController.userId, t.getId() });
	}

	@Override
	public int delete(int id) throws Exception {
		String sql = "DELETE FROM doc2_sub_module WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { id });
	}

	@Override
	public SubModuleResponseVo getById(int id, String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT sm.id, sm.name, concat(sm.module_id, '->',  m.name) module");
			sql.append(" , u1.username created_by, sm.created_date");
			sql.append(" , u2.username updated_by, sm.updated_date");
		sql.append(" FROM doc2_sub_module sm");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = sm.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = sm.updated_by )");
		sql.append(" WHERE sm.id = ?");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id}, new BeanPropertyRowMapper<>(SubModuleResponseVo.class));
	}

	@Override
	public List<SubModuleResponseVo> listAll(String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT sm.id, sm.name, concat(sm.module_id, '->',  m.name) module");
			sql.append(" , u1.username created_by, sm.created_date");
			sql.append(" , u2.username updated_by, sm.updated_date");
		sql.append(" FROM doc2_sub_module sm");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = sm.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = sm.updated_by )");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(SubModuleResponseVo.class));
	}

	@Override
	public List<SubModuleResponseVo> listLimited(String userId, String userRole, int limit) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT sm.id, sm.name, concat(sm.module_id, '->',  m.name) module");
			sql.append(" , u1.username created_by, sm.created_date");
			sql.append(" , u2.username updated_by, sm.updated_date");
		sql.append(" FROM doc2_sub_module sm");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = sm.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = sm.updated_by )");
		sql.append(" LIMIT ?");
		return jdbcTemplate.query(sql.toString(), new Object[] { limit }, new BeanPropertyRowMapper<>(SubModuleResponseVo.class));
	}

}

package com.documentary.admin.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.vo.AuditVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Audit Repository"
 */
@Repository
public class AuditRepository {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	public List<AuditVo> listAudit(Timestamp fromTimestamp, Timestamp toTimestamp) throws Exception{
		String sql = "SELECT id, username, state, statetimestamp from sys_audit where statetimestamp between ? and ?";
		return jdbcTemplate.query(sql, new Object[] {fromTimestamp, toTimestamp}, new BeanPropertyRowMapper<>(AuditVo.class));
	}

}

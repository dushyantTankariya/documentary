package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.ScreenRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.request.ScreenRequestVo;
import com.documentary.vo.response.ScreenResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Screen Management Service Implementation"
 *
 */
@Component
public class ScreenBo extends AbstractBoImpl<ScreenRequestVo, ScreenResponseVo>  {

	@Autowired
	public ScreenBo(ScreenRepository screenRepository) {
		super(screenRepository);
	}

}

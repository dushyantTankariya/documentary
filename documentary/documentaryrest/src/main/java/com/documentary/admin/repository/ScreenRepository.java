package com.documentary.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.admin.controller.ScreenController;
import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.request.ScreenRequestVo;
import com.documentary.vo.response.ScreenResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Screen Management Repository"
 */
@Repository
public class ScreenRepository implements CommonRepository<ScreenRequestVo, ScreenResponseVo>{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insert(ScreenRequestVo t) throws Exception {
		String sql = "INSERT INTO doc3_screen (name, version, submodule_id, created_by, created_date) VALUES (?, ?, ?, ?, SYSDATE())";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), t.getVersion(), t.getSubmoduleId(), ScreenController.userId });
	}

	@Override
	public int update(ScreenRequestVo t) throws Exception {
		String sql = "UPDATE doc3_screen SET name = ?, version = ?, submodule_id = ?, updated_by = ?, updated_date = SYSDATE() WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), t.getVersion(), t.getSubmoduleId(), ScreenController.userId, t.getId() });
	}

	@Override
	public int delete(int id) throws Exception {
		String sql = "DELETE FROM doc3_screen WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { id });
	}

	@Override
	public ScreenResponseVo getById(int id, String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT s.id, s.name, s.version");
			sql.append(" , concat(s.submodule_id, '->',  sm.name, '->', sm.module_id, '->', m.name) submodule");
			sql.append(" , u1.username created_by, s.created_date");
			sql.append(" , u2.username updated_by, s.updated_date");
		sql.append(" FROM doc3_screen s");
		sql.append(" LEFT OUTER JOIN doc2_sub_module sm ON ( sm.id = s.submodule_id )");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = s.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = s.updated_by )");
		sql.append(" WHERE s.id = ?");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id}, new BeanPropertyRowMapper<>(ScreenResponseVo.class));
	}

	@Override
	public List<ScreenResponseVo> listAll(String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT s.id, s.name, s.version");
			sql.append(" , concat(s.submodule_id, '->',  sm.name, '->', sm.module_id, '->', m.name) submodule");
			sql.append(" , u1.username created_by, s.created_date");
			sql.append(" , u2.username updated_by, s.updated_date");
		sql.append(" FROM doc3_screen s");
		sql.append(" LEFT OUTER JOIN doc2_sub_module sm ON ( sm.id = s.submodule_id )");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = s.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = s.updated_by )");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(ScreenResponseVo.class));
	}

	@Override
	public List<ScreenResponseVo> listLimited(String userId, String userRole, int limit) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT s.id, s.name, s.version");
			sql.append(" , concat(s.submodule_id, '->',  sm.name, '->', sm.module_id, '->', m.name) submodule");
			sql.append(" , u1.username created_by, s.created_date");
			sql.append(" , u2.username updated_by, s.updated_date");
		sql.append(" FROM doc3_screen s");
		sql.append(" LEFT OUTER JOIN doc2_sub_module sm ON ( sm.id = s.submodule_id )");
		sql.append(" LEFT OUTER JOIN doc1_module m ON ( m.id = sm.module_id )");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = s.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = s.updated_by )");
		sql.append(" LIMIT ?");
		return jdbcTemplate.query(sql.toString(), new Object[] { limit }, new BeanPropertyRowMapper<>(ScreenResponseVo.class));
	}

}

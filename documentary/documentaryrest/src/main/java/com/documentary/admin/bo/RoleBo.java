package com.documentary.admin.bo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.admin.repository.RoleRepository;
import com.documentary.common.bo.abstractImpl.AbstractBoImpl;
import com.documentary.vo.UserRoleVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Role Management Service Implementation"
 *
 */
@Component
public class RoleBo extends AbstractBoImpl<UserRoleVo, UserRoleVo> {

	@Autowired
	public RoleBo(RoleRepository roleRepository) {
		super(roleRepository);
	}

}

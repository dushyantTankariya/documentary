package com.documentary.admin.repository;

import static com.documentary.enums.ActiveDeleteEnum.DEACTIVE;
import static com.documentary.enums.ActiveDeleteEnum.DELETE;
import static com.documentary.enums.ActiveDeleteEnum.AVAILABLE;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.UserLoginVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Management Repository"
 */
@Repository
public class UserMgmtRepository implements CommonRepository<UserLoginVo, UserLoginVo>{

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Override
	public int insert(UserLoginVo t) throws Exception {
		String sql = "INSERT INTO doc5_user (firstname, lastname, username, password, email, role_id) VALUES (?, ?, ?, ?, ?, ?)";
        return jdbcTemplate.update(sql, t.instanceFactory());
	}

	@Override
	public int update(UserLoginVo t) throws Exception{
		String sql = "UPDATE doc5_user SET firstname = ?, lastname = ?, username = ?, email = ?, role_id = ?, is_active = ? WHERE id = ?";
        return jdbcTemplate.update(sql, t.updateInstanceFactory());
	}

	@Override
	public int delete(int id) throws Exception {
		String sql = "UPDATE doc5_user SET is_active = ?, is_delete = ? WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] {DEACTIVE.value, DELETE.value, id});
	}

	@Override
	public UserLoginVo getById(int id, String userId, String userRole) throws Exception {
		return null;
	}

	@Override
	public List<UserLoginVo> listAll(String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT distinct u.id, u.firstname, u.lastname, u.username, u.email ,");
			sql.append(" concat(u.role_id, '->', r.name, '->', group_concat( s.name , '-' , r.access)) as role ,");
			sql.append(" u.is_active as isActive");
		sql.append(" FROM doc5_user u");
		sql.append(" LEFT OUTER JOIN doc4_role r on (r.role_code = u.role_id)");
		sql.append(" LEFT OUTER JOIN doc3_screen s on (s.id = r.screen)");
		sql.append(" WHERE u.is_delete = ?");
		sql.append(" group by u.id, u.firstname, u.lastname, u.username, u.email ,u.is_active, r.name");
	
		return jdbcTemplate.query(sql.toString(), new Object[] {AVAILABLE.value}, new BeanPropertyRowMapper<>(UserLoginVo.class));
	}

	@Override
	public List<UserLoginVo> listLimited(String userId, String userRole, int limit) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT distinct u.id, u.firstname, u.lastname, u.username, u.email ,");
			sql.append(" concat(u.role_id, '->', r.name, '->', group_concat( s.name , '-' , r.access)) as role ,");
			sql.append(" u.is_active as isActive");
		sql.append(" FROM doc5_user u");
		sql.append(" LEFT OUTER JOIN doc4_role r on (r.role_code = u.role_id)");
		sql.append(" LEFT OUTER JOIN doc3_screen s on (s.id = r.screen)");
		sql.append(" WHERE u.id = ? and r.name = 'admin' and u.is_delete = ?");
		sql.append(" group by u.id, u.firstname, u.lastname, u.username, u.email ,u.is_active, r.name");
		sql.append(" LIMIT ?");
		return jdbcTemplate.query(sql.toString(),new Object[] {userId, AVAILABLE.value, limit}, new BeanPropertyRowMapper<>(UserLoginVo.class));
	}
	
	
}

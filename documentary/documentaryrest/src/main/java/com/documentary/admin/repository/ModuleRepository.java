package com.documentary.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.admin.controller.ModuleController;
import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.request.ModuleRequestVo;
import com.documentary.vo.response.ModuleResponseVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Module Management Repository"
 */
@Repository
public class ModuleRepository implements CommonRepository<ModuleRequestVo, ModuleResponseVo>{

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insert(ModuleRequestVo t) throws Exception {
		String sql = "INSERT INTO doc1_module (name, created_by, created_date) VALUES (?, ?, SYSDATE())";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), ModuleController.userId });
	}

	@Override
	public int update(ModuleRequestVo t) throws Exception {
		String sql = "UPDATE doc1_module SET name = ?, updated_by = ?, updated_date = SYSDATE() WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { t.getName(), ModuleController.userId, t.getId() });
	}

	@Override
	public int delete(int id) throws Exception {
		String sql = "DELETE FROM doc1_module WHERE id = ?";
		return jdbcTemplate.update(sql, new Object[] { id });
	}

	@Override
	public ModuleResponseVo getById(int id, String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT m.id, m.name");
			sql.append(" , u1.username created_by, m.created_date");
			sql.append(" , u2.username updated_by, m.updated_date");
		sql.append(" FROM doc1_module m");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = m.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = m.updated_by )");
		sql.append(" WHERE m.id = ?");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id}, new BeanPropertyRowMapper<>(ModuleResponseVo.class));
	}

	@Override
	public List<ModuleResponseVo> listAll(String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT m.id, m.name");
			sql.append(" , u1.username created_by, m.created_date");
			sql.append(" , u2.username updated_by, m.updated_date");
		sql.append(" FROM doc1_module m");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = m.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = m.updated_by )");
		return jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(ModuleResponseVo.class));
	}

	@Override
	public List<ModuleResponseVo> listLimited(String userId, String userRole, int limit) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT m.id, m.name");
			sql.append(" , u1.username created_by, m.created_date");
			sql.append(" , u2.username updated_by, m.updated_date");
		sql.append(" FROM doc1_module m");
		sql.append(" LEFT OUTER JOIN doc5_user u1 ON ( u1.id = m.created_by )");
		sql.append(" LEFT OUTER JOIN doc5_user u2 ON ( u2.id = m.updated_by )");
		sql.append(" LIMIT ?");
		return jdbcTemplate.query(sql.toString(), new Object[] { limit }, new BeanPropertyRowMapper<>(ModuleResponseVo.class));
	}

}

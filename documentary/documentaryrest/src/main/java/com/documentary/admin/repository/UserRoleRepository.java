package com.documentary.admin.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.documentary.common.repository.CommonRepository;
import com.documentary.vo.UserRoleVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "User Role Repository"
 */
@Repository
public class UserRoleRepository implements CommonRepository<UserRoleVo, UserRoleVo>{
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public int insert(UserRoleVo t) throws Exception {
		String sql = "INSERT INTO doc4_role (role_code, name, screen, access) VALUES (?, ?, ?)";
        return jdbcTemplate.update(sql, t.instanceFactory());
	}

	@Override
	public int update(UserRoleVo t) throws Exception {
		String sql = "UPDATE doc4_role SET role_code, name = ?, screen = ?, access = ?, WHERE id = ?";
        return jdbcTemplate.update(sql, t.updateInstanceFactory());
	}

	@Override
	public int delete(int id) throws Exception {
		String sql = "DELETE FROM doc4_role where id = ?";
		return jdbcTemplate.update(sql, id);
	}

	@Override
	public UserRoleVo getById(int id, String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT r.id, r.role_code, r.name, concat(r.screen, '-' , s.name) as screen, r.access");
		sql.append(" FROM doc4_role r");
		sql.append(" LEFT OUTER JOIN doc3_screen s ON (s.id = r.screen)");
		sql.append(" WHERE id = ?");
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {id}, new BeanPropertyRowMapper<>(UserRoleVo.class));
	}

	@Override
	public List<UserRoleVo> listAll(String userId, String userRole) throws Exception {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT role_code, r.name, (group_concat(concat( s.name, '-'), r.access) ) as screen, r.access as rights");
		sql.append(" FROM doc4_role r");
		sql.append(" LEFT OUTER JOIN doc3_screen s ON (s.id = r.screen)");
		sql.append(" group by r.role_code, r.name, r.access");
		
		
		
		/*sql.append("SELECT r.id, r.role_code, r.name, concat(r.screen, '-', s.name) as screen, r.access");
		sql.append(" FROM doc4_role r");
		sql.append(" LEFT OUTER JOIN doc3_screen s ON (s.id = r.screen)");*/
		List<UserRoleVo> lst =jdbcTemplate.query(sql.toString(), new BeanPropertyRowMapper<>(UserRoleVo.class)); 
		return lst;
	}

	@Override
	public List<UserRoleVo> listLimited(String userId, String userRole, int limit) throws Exception {
		StringBuilder sql = new StringBuilder();
		/*sql.append("SELECT r.id, r.role_code, r.name, concat(r.screen, '-', s.name) as screen, r.access");
		sql.append(" FROM doc4_role r");
		sql.append(" LEFT OUTER JOIN doc3_screen s ON (s.id = r.screen)");*/
		sql.append("SELECT role_code, r.name, (group_concat(concat(r.screen, '-', s.name), '->' ) ) as screen, r.access");
		sql.append(" FROM doc4_role r");
		sql.append(" LEFT OUTER JOIN doc3_screen s ON (s.id = r.screen)");
		sql.append(" group by r.role_code, r.name, r.access");
		sql.append(" limit ?");
		return jdbcTemplate.query(sql.toString(), new Object[] { limit }, new BeanPropertyRowMapper<>(UserRoleVo.class));
	}

}

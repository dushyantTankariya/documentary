package com.documentary.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

import com.documentary.vo.response.impl.BaseResponseImplVo;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.util.auth.Authorization;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Transaction Filter"
 */
@Configuration
public class TransactionFilter implements Filter {

	/**
	 * @throws ServletException
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig filterConfig) throws ServletException { }

	/**
	 * @throws IOException, ServletException
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		
		if (!httpRequest.getPathInfo().contains("user") && !(new Authorization().claim(httpRequest.getHeader("Authorization")))) {
			((HttpServletResponse) response).setHeader("Content-Type", "application/json");
            ((HttpServletResponse) response).setStatus(HttpStatus.UNAUTHORIZED.value());
            response.getOutputStream().write(restResponseBytes(new BaseResponseImplVo(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED)));
            return;
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() { }

	/**
	 * @param eErrorResponse
	 * @return byte array of JSON
	 * @throws IOException
	 */
	private byte[] restResponseBytes(BaseResponseImplVo eErrorResponse) throws IOException {
		String serialized = new ObjectMapper().writeValueAsString(eErrorResponse);
		return serialized.getBytes();
	}
}

package com.documentary.helper;

import static com.documentary.enums.UserRoleEnum.WRITER;

import org.omg.CORBA.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.documentary.user.repository.DocumentaryRepositoryImpl;
import com.documentary.user.repository.UserRepositoryImpl;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Documentary Helper"
 */
@Component
public class DocumentaryHelper {
	
	/**
	 * @see "User Repository Implementation"
	 */
	@Autowired
	UserRepositoryImpl userRepositoryImpl;
	
	/**
	 * @see "Documentary Repository Implementation"
	 */
	@Autowired
	DocumentaryRepositoryImpl documentaryRepositoryImpl;

	/**
	 * @see "To verify the request is valid with writer role"
	 * @param a userId in the form of {@link Integer}
	 * @return a flag value in the form of {@link Boolean}
	 * @throws Any Exception
	 */
	public Boolean isValidWriter(Integer userId) throws Exception {
		return userRepositoryImpl.getById(userId, "", "").getRole().equalsIgnoreCase(WRITER.name());
	}
	
	
	/**
	 * @see "To verify the update request is valid with writer role"
	 * @param a postId in the form of {@link Integer} 
	 * @param a userId in the form of {@link Integer}
	 * @param a userRole in the form of {@link String}
	 * @return a flag value in the form of {@link Boolean}
	 * @throws Any Exception
	 */
	public Boolean isValidWriterToUpdate(Integer postId, Integer userId, String userRole) throws Exception {
		
		return  userRepositoryImpl.getById(userId, "", "").getRole().equalsIgnoreCase(WRITER.name())
			&&  documentaryRepositoryImpl.getById(postId, ""+userId, userRole) != null;
	}
}

package com.documentary.helper;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.documentary.vo.response.ResponseVo;
import com.documentary.vo.response.impl.BaseDataResponseImplVo;
import com.documentary.vo.response.impl.BaseResponseImplVo;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Common Helper class to build Response"
 */
@Component
public class CommonHelper {

	/**
	 * @see "Build a base Response"
	 * @param No. of row affected
	 * @return a Response in the form of {@link ResponseVo}
	 */
	public ResponseVo buildBaseResponse(int row) {
		BaseResponseImplVo baseResponse;
		if(row>0) { //success
			baseResponse = new BaseResponseImplVo(HttpStatus.OK.value(), HttpStatus.OK);
		}else { //Failed
			baseResponse = new BaseResponseImplVo(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN);
		}
		return baseResponse;
	}
	
	/**
	 * @see "Build a base Response for Data"
	 * @param a o in the form of {@link Object}
	 * @return a aResponse in the form of {@link BaseDataResponseImplVo}
	 */
	public BaseDataResponseImplVo buildBaseDataResponse(Object o) {
		BaseDataResponseImplVo  bdr;
		if(o!=null) { //success
			bdr = new BaseDataResponseImplVo(HttpStatus.OK.value(), HttpStatus.OK, o);
		}else { //Failed
			bdr = new BaseDataResponseImplVo(HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN, o);
		}
		return bdr;
	}
}

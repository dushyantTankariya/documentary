package com.documentary.helper;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

import org.springframework.stereotype.Component;

/**
 * @author Dushyant Tankariya
 * @since 1.0.0.0
 * @see "Dashboard Helper"
 */
@Component
public class DashboardHelper {

	/**
	 * @return A Disk Space long value
	 */
	public static Long calculateDiskSpace() throws Exception{
		File file = new File("/");
		double free = file.getFreeSpace() / (1024.0 * 1024 * 1024);
		double max =  file.getTotalSpace() / (1024.0 * 1024 * 1024);
		double used = (max - free);
		return  (long) ((used * 100) / max);
	}
	
	/**
	 * @return A Calculated Long value
	 */
	public static Long calculateHeapMemory() throws Exception{
		MemoryMXBean m = ManagementFactory.getMemoryMXBean();
		long max = m.getHeapMemoryUsage().getMax();
		long used = m.getHeapMemoryUsage().getUsed();
		return ((used * 100) / max);
	}
}

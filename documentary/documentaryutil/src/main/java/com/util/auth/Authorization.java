package com.util.auth;

import static com.util.enums.AuthEnum.SHA_KEY;

import java.util.Calendar;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class Authorization {

	public Authorization() {}
	
	/* =======Authenticate================================================ */
	public String buildJwtToken(String username, String password, String remeber) {
		String token = null;
		
		try {
			//The JWT signature algorithm we will be using to sign the token
			Calendar calendar = Calendar.getInstance();
			if(remeber.equalsIgnoreCase("on")) {
				calendar.add(Calendar.DATE, 1);
			}else {
				calendar.add(Calendar.MINUTE, 30);
			}
			token = Jwts.builder().setSubject("token")
					.setExpiration(calendar.getTime())
					.setIssuer("dushyant.tankariya@gmail.com")
					.claim("groups", new String[] { username, password })
					// HMAC using SHA-512  and 12345678 base64 encoded
					.signWith(SignatureAlgorithm.HS512, SHA_KEY.value).compact();
			
		}catch(Exception e) {
			throw new RuntimeException("Internal Server Error");
		}
		
		return token;
	}
	
	public boolean claim(String token) {
		boolean claimFlag = false;
		try {
			Jwts.parser().setSigningKey(SHA_KEY.value).parseClaimsJws(token).getBody();
//			Jwts.parser().setSigningKey(SHA_KEY.value).parseClaimsJws(token).getBody().setExpiration(null).getSubject();
//			Jwts.parser().setSigningKey(SHA_KEY.value).parseClaimsJws(token).getBody().getExpiration();
			claimFlag = true;
		}catch(Exception e) {
			claimFlag = false;
		}
		return claimFlag;
	}

}

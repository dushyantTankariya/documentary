<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Clean Blog - Start Bootstrap Theme</title>

  <!-- Bootstrap core CSS -->
  <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="${pageContext.request.contextPath}/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">

  <!-- Editor -->
  <link href="${pageContext.request.contextPath}/resources/css/editor.css" rel="stylesheet">
  
  <!-- Custom styles for this template -->
  <link href="${pageContext.request.contextPath}/resources/css/clean-blog.min.css" rel="stylesheet">
  
  
</head>

<body>

  <!-- Navigation -->
  <jsp:include page="navigation"/>
  
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('${pageContext.request.contextPath}/resources/img/post-bg.jpg')">
    <div class="overlay"></div>
    
  </header>

  <!-- Post Content -->
  <article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto description">
         
         
        </div>
      </div>
    </div>
  </article>
  
  <hr>
  
  <!-- Footer -->
  <jsp:include page="footer"/>

	<!-- Create Post -->
	<!-- Modal -->
	<div class="modal fade" id="postModal" tabindex="-1" role="dialog"
		aria-labelledby="postModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="postModalLabel">Post</h5>
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<form id="create-post-form" method="post" role="form"
					style="display: block;">
					<div class="modal-body">
						<!-- <input type="hidden" name="id" id="id"> -->
						<div class="form-group">
							<input type="text" name="title" id="title" tabindex="1"
								class="form-control" placeholder="title" value="">
						</div>
						<div class="form-group">
							<label for="description">Description</label>
							<textarea id="description"></textarea>
						</div>

					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary"
							data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save
							changes</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- Bootstrap core JavaScript -->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  
  

  <!-- Editor -->
  <script src="${pageContext.request.contextPath}/resources/js/editor.js"></script>
  
  <!-- post routing -->
  <script src="${pageContext.request.contextPath}/resources/routing/post.js"></script>
  
  <!-- Custom scripts for this template -->
  <script defer src="${pageContext.request.contextPath}/resources/js/clean-blog.min.js"></script>
</body>

</html>

<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Health</title>

  <!-- Custom fonts for this template-->
  <link href="${pageContext.request.contextPath}/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/resources/css/ops.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

     <!-- sidebar -->
   <jsp:include page="opsSidebar"/>
   <jsp:useBean id="now" class="java.util.Date"/>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
		  
		  <div class="mx-auto" >
			<h3 >Health V-1.0.0.0</h3>
		  </div>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
			<!-- Content Row -->
          <div class="row">
			<table class="table">
			  <thead>
				<tr>
				  <th scope="col">Service name</th>
				  <th scope="col">Status</th>
				  <th scope="col">Details</th>
				</tr>
			  </thead>
			  <tbody>
				<tr>
				  <td>Database</td>
				  <td><span class="badge badge-success" id="db">UP</span></td>
				  <td> <span class="fa fa-eye fa-w-18" data-toggle="modal" data-target="#dbModal" role="button"></span></td>
				  
				</tr>
				<tr>
				  <td>Disk Space</td>
				  <td><span class="badge badge-success" id="disk">UP</span></td>
				  <td> <span class="fa fa-eye fa-w-18" data-toggle="modal" data-target="#diskSpaceModal" role="button"></span></td>
				</tr>
				<tr>
				  <td>Application</td>
				  <td><span class="badge badge-success" id="application">UP</span></td>
				  <td></td>
				</tr>
			  </tbody>
			</table>			
		  </div>
		</div>
		
		<div class="modal fade" id="dbModal" tabindex="-1" role="dialog" aria-labelledby="dbModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="dbModalLabel">Database Properties</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<table class="table">
						  <thead>
							<tr>
							  <th scope="col">Name</th>
							  <th scope="col">Value</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <td>Database</td>
							  <td id="dbName"></td>
							</tr>
							<tr>
							  <td>Result</td>
							  <td id="result"></td>
							</tr>
							<tr>
							  <td>Validation Query</td>
							  <td id="query"></td>
							</tr>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
	  </div>
	  <div class="modal fade" id="diskSpaceModal" tabindex="-1" role="dialog" aria-labelledby="diskSpaceModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="diskSpaceModalLabel">Disk space properties</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<table class="table">
						  <thead>
							<tr>
							  <th scope="col">Name</th>
							  <th scope="col">Value</th>
							</tr>
						  </thead>
						  <tbody>
							<tr>
							  <td>total</td>
							  <td id="totalSpace"></td>
							</tr>
							<tr>
							  <td>free</td>
							  <td id="freeSpace"></td>
							</tr>
							<tr>
							  <td>Threshold</td>
							  <td id="thresholdSpace"></td>
							</tr>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
	  </div>
      <!-- Footer -->
 	 <footer class="sticky-footer bg-white">
       <div class="container my-auto">
         <div class="copyright text-center my-auto">
			<span>Developed by Dushyant Tankariya <fmt:formatDate pattern = "yyyy" value = "${now}" /> </span>
         </div>
       </div>
     </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/js/ops.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/routing/health.js"></script>

</body>

</html>

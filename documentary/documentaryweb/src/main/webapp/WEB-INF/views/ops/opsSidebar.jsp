
<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="dashboard">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">Documentary</div>
	</a>

	<!-- Divider -->
	<hr class="sidebar-divider my-0">

	<!-- Nav Item - Dashboard -->
	<li class="nav-item dashboard">
		<a class="nav-link" href="dashboard"> 
			<i class="fas fa-fw fa-tachometer-alt"></i> 
			<span>Dashboard</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">Administration</div>
	
	<li class="nav-item userMgmt">
		<a class="nav-link collapsed" href="userMgmt">
			<i class="fas fa-fw fa-users"></i> 
			<span>User</span>
		</a>
	</li>
	
	<li class="nav-item audits">
		<a class="nav-link collapsed" href="audits">
			<i class="fas fa-fw fa-history"></i> 
			<span>Audits</span>
		</a>
	</li>
	
	<li class="nav-item dropdown advance">
		<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">
			<i class="fas fa-fw fa-briefcase"></i> 
			<span>Advance</span>
		</a>
		<ul class="dropdown-menu">
		    <li><a class="dropdown-item module" href="module"> 
		    		<i class="fas fa-fw fa-th-large"></i> 
		    		<span>Module</span>
		    	</a>
		    </li>
		    <li><a class="dropdown-item submodule" href="submodule"> 
		    		<i class="fas fa-fw fa-th"></i>
		    		<span>Sub Module</span>
		    	</a>
		    </li>
		    <li><a class="dropdown-item screen" href="screen"> 
		    		<i class="fas fa-fw fa-desktop"></i>
		    		<span>Screen</span> 
		    	</a>
		    </li>
		    <li><a class="dropdown-item role" href="role"> 
		    		<i class="fas fa-fw fa-user-secret"></i>
		    		<span>Role</span> 
		    	</a>
		    </li>
	    </ul>
		
	</li>
	
	<!-- Divider -->
	<hr class="sidebar-divider">

	<!-- Heading -->
	<div class="sidebar-heading">Interface</div>

	<!-- Nav Item - Pages Collapse Menu -->
	<li class="nav-item health">
		<a class="nav-link collapsed" href="health"> 
			<i class="fas fa-fw fa-heart"></i> 
			<span>Health</span>
		</a>
	</li>

	<li class="nav-item logs">
		<a class="nav-link collapsed" href="logs">
			<i class="fas fa-fw fa-tasks"></i>
			<span>logs</span>
		</a>
	</li>

	
	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">

	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>

</ul>
<script>
(function() {
		var elems = document.querySelector(".active");
		if (elems !== null) {
			elems.classList.remove("active");
		}
		var element = window.location.pathname.split("/")[2];
		
		document.querySelector("." + element).classList.add("active");
		
		if(element == 'module' || element == 'submodule' || element == 'screen' || element == 'role' ){
			document.querySelector(".advance").classList.add("active");	
		}
})();
</script>
<!-- End of Sidebar -->
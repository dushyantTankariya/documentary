<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Dashboard</title>

  <!-- Custom fonts for this template-->
  <link href="${pageContext.request.contextPath}/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/resources/css/ops.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

   <!-- sidebar -->
   <jsp:include page="opsSidebar"/>
   <jsp:useBean id="now" class="java.util.Date"/>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

		<!-- Topbar -->
		<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
		
			<!-- Sidebar Toggle (Topbar) -->
			<button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
				<i class="fa fa-bars"></i>
			</button>
		
			<div class="mx-auto">
				<h3>Dashboard V-1.0.0.0</h3>
			</div>
		
		</nav>
		<!-- End of Topbar -->
        
        <!-- Begin Page Content -->
        <div class="container-fluid">
		  
		  <!-- Content Row -->
          <div class="row">
			
			<div class="col-xl-7 col-lg-6">
			  <!-- Project Card Example -->
			  <div class="card shadow mb-3">
				<div class="card-header py-3">
				  <h6 class="m-0 font-weight-bold text-primary">Disk Space & Memory</h6>
				</div>
				<div class="card-body">
				  <h4 class="small font-weight-bold">Disk Space<span class="float-right" id= "diskSpan">0%</span></h4>
				  <div class="progress mb-3">
					<div class="progress-bar bg-info" role="progressbar" style="width: 0%" id="diskDiv" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"></div>
				  </div>
				  <h4 class="small font-weight-bold">Heap Memory <span class="float-right" id="heapSpan">0%</span></h4>
				  <div class="progress mb-3">
					<div class="progress-bar bg-warning" role="progressbar" style="width: 0%" id="heapDiv" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"></div>
				  </div>
				</div>
			  </div>
				<!-- Content Row -->
				  <div class="row">

					<!-- Content Column -->
					<div class="col-lg-10 mb-9">
						<div class="card shadow mb-4">
							<div class="card-header py-3">
							  <h6 class="m-0 font-weight-bold text-primary">Thread Info</h6>
							</div>
							<div class="card-body">
								<div class="row">
								<div class="col-lg-5 mb-3">
								  <div class="card bg-success text-white shadow">
									<div class="card-body">
									  Active Count
									  <div class="text-white-50 small" id="activeCount">0</div>
									</div>
								  </div>
								</div>
								<div class="col-lg-5 mb-3">
								  <div class="card bg-secondary text-white shadow">
									<div class="card-body">
									  Daemon Count
									  <div class="text-white-50 small" id="daemonCount">0</div>
									</div>
								  </div>
								</div>
								</div>
								<div class="row">
								<div class="col-lg-10 mb-3">
								  <div class="card bg-primary text-white shadow">
									<div class="card-body">
									  Total Started Count
									  <div class="text-white-50 small" id="totalStartedCount">0</div>
									</div>
								  </div>
								</div>
								</div>
							</div>
						</div>
					</div>
				</div>
		    </div>
			<div class="col-lg-5 mb-4">
              <!-- Illustrations -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">System Information</h6>
                </div>
                <div class="card-body">
                  <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="${pageContext.request.contextPath}/resources/img/system.svg" alt="">
                  </div>
                  
				    <div class="row border-left-info">
						<label for="osName" class="col-sm-4 col-form-label">Name</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="osName" placeholder="OS Name">
						</div>
					</div><hr>
					<div class="row border-1 border-left-info">
						<label for="osVersion" class="col-sm-4 col-form-label">Version</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="osVersion" placeholder="OS Version">
						</div>
					</div><hr>
					<div class="row border-1 border-left-info">
						<label for="osLoadAvg" class="col-sm-4 col-form-label">Load Avg</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="osLoadAvg" placeholder="OS Load Average">
						</div>
					</div><hr>
					<div class="row border-1 border-left-info">
						<label for="processors" class="col-sm-4 col-form-label">Processors</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="processors" placeholder="Available Processors">
						</div>
					</div><hr>
					<div class="row border-1 border-left-info">
						<label for="upTime" class="col-sm-4 col-form-label">Up Time</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="upTime" placeholder="Up Time">
						</div>
					</div><hr>
					<div class="row border-1 border-left-info">
						<label for="startTime" class="col-sm-4 col-form-label">Start Time</label>
						<div class="col-sm-7">
						  <input disabled="disabled" type="text" class="form-control" id="startTime" placeholder="Start Time">
						</div>
					</div>
					
                </div>
              </div>
			</div>
		  </div>
		</div>
     <!-- Footer -->
	<footer class="sticky-footer bg-white">
		<div class="container my-auto">
			<div class="copyright text-center my-auto">
				<span>Developed by Dushyant Tankariya <fmt:formatDate
						pattern="yyyy" value="${now}" />
				</span>
			</div>
		</div>
	</footer>
	<!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/js/ops.min.js"></script>
    
  <!-- Custom routing scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/routing/dashboard.js"></script>

</body>

</html>

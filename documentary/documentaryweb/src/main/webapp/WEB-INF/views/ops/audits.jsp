<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Audits</title>

  <!-- Custom fonts for this template-->
  <link href="${pageContext.request.contextPath}/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/resources/css/ops.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

     <!-- sidebar -->
   <jsp:include page="opsSidebar"/>
   <jsp:useBean id="now" class="java.util.Date"/>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
		  
		  <div class="mx-auto" >
			<h3 >Audits V-1.0.0.0</h3>
		  </div>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
			<!-- Content Row -->
          <div class="row">
			<div class="col-md-6"> 
				<h4>Filter per date</h4> 
				<div class="input-group mb-4"> 
					<div class="input-group-prepend"> 
						<span class="input-group-text" >from</span> 
					</div> 
					<input type="date" name="start"  id="start" class="form-control" value="<fmt:formatDate pattern = "yyyy-MM-dd" value = "${now}" />"> 
				
					<div class="input-group-append"> 
						<span class="input-group-text" >to</span> 
					</div> 
					<input type="date" name="end" id="end" class="form-control" value="<fmt:formatDate pattern = "yyyy-MM-dd" value = "${now}" />">  
				</div> 
			</div>		
		  </div>
		  <div class="row">
			<table id="auditTable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
				<thead>
					<tr> 						
						<th class="th-sm">User</th>
						<th class="th-sm">State</th>
						<th class="th-sm">Date</th>
					</tr>
				</thead>
				<tbody>
					
				</tbody>
			</table>
		  </div>
		</div>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
				<span>Developed by Dushyant Tankariya <fmt:formatDate pattern = "yyyy" value = "${now}" /> </span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Bootstrap core JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/js/ops.min.js"></script>
  
  <script src="${pageContext.request.contextPath}/resources/routing/audit.js"></script>

</body>

</html>

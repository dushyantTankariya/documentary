<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sub Module Management</title>

  <!-- Custom fonts for this template-->
  <link href="${pageContext.request.contextPath}/resources/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Bootstrap --> 
  <link href="${pageContext.request.contextPath}/resources/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
  
  <!-- DataTables -->
  <link href="${pageContext.request.contextPath}/resources/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
   
  <!-- Tags Input -->
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.css" rel="stylesheet"/>
  <!--  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput-typeahead.css" rel="stylesheet"/> -->
   
  <!-- Custom styles for this template-->
  <link href="${pageContext.request.contextPath}/resources/css/ops.min.css" rel="stylesheet">
  <link href="${pageContext.request.contextPath}/resources/css/adm.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

     <!-- sidebar -->
   <jsp:include page="opsSidebar"/>
   <jsp:useBean id="now" class="java.util.Date"/>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>
		  
		  <div class="mx-auto" >
			<h3 >Sub Module Management V-1.0.0.0</h3>
		  </div>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
			<!-- Content Row -->
          	 <div class="row">
         		<div class="col">
					<button type="button" id="newRecordId" title="Add New Record"
					 	data-toggle="modal" data-target="#subModuleModal" 
						class="new-rounded btn btn-success form-control">+</button>
					<table class="table table-striped table-bordered" cellspacing="0" width="100%">
					    <thead>
					      <tr>
					        <th class="th-sm">Name</th>
					        <th class="th-sm">Module</th>
					        <th class="th-sm">Created By</th>
					        <th class="th-sm">created Date</th>
					        <th class="th-sm">updated By</th>
					        <th class="th-sm">Updated Date</th>
					        <th class="th-sm">Action</th>
					      </tr>
					    </thead>
					    <tbody> </tbody>
					    <tfoot>
					      <tr>
							<th class="th-sm">Name</th>
							<th class="th-sm">Module</th>
					        <th class="th-sm">Created By</th>
					        <th class="th-sm">created Date</th>
					        <th class="th-sm">updated By</th>
					        <th class="th-sm">Updated Date</th>
					        <th class="th-sm">Action</th>
					      </tr>
					    </tfoot>
					  </table>
		 		</div>
          </div>
          	<!-- Row End's here -->
		</div>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
				<span>Developed by Dushyant Tankariya <fmt:formatDate pattern = "yyyy" value = "${now}" /> </span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->
	</div>
  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
  
  <!-- Sub Module Modal -->
   <div class="modal fade" id="subModuleModal" tabindex="-1" role="dialog" aria-labelledby="subModuleModalLabel" aria-hidden="true">
   	<div class="modal-dialog modal-lg" role="document">
   		<div class="modal-content">
   			<div class="modal-header">
				<h5 class="modal-title" id="subModuleModalLabel">New Sub Module</h5>
				<button type="button" id="closeSubModuleModel" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="subModuleForm" method="post" action="/" role="form" style="display: block;">
					<input type="hidden" name="id" class="form-control" value="0"/>
					<div class="row">
						<div class="col-sm">
							<lable>Module<select id="moduleId" name="moduleId" class="form-control selectpicker" data-live-search="true"></select>
							</lable>
						</div>
					</div>
					<div class="row">
						<div class="col-sm">
							<lable>Name<input type="text" id="nameId" name="name" class="form-control" required/>
							</lable>
						</div>
					</div>
					<div class="row">
						<div class="col-sm text-right mt-3">
							<button type="submit" class="btn btn-success">Submit</button>
							<button type="reset" class="btn btn-secondary">Reset</button>
							<button type="button" class="btn btn-dark" data-dismiss="modal" aria-label="Close">Cancel</button>
						</div>
					</div>
				</form>
			</div>
   		</div>
   	</div>
   </div>
	
  <!-- Bootstrap and Jquery -->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery/jquery.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>
  
  <!-- DataTables -->
  <script src="${pageContext.request.contextPath}/resources/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/vendor/datatables/dataTables.bootstrap4.min.js"></script>
  
  <!-- Tags Input -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
  
  <!-- Core plugin JavaScript-->
  <script src="${pageContext.request.contextPath}/resources/vendor/jquery-easing/jquery.easing.min.js"></script>
  
  <!-- Custom scripts for all pages-->
  <script src="${pageContext.request.contextPath}/resources/js/ops.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/routing/subModule.js"></script>  
</body>

</html>

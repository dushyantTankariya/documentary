/* Load Auth.js File before document is ready. */
$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
    $.getScript( "resources/routing/util.js", function() {
        $.holdReady( false );
    });
});
$(document).ready(function () {
    //Ajax header setup for every request 
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    
    
    if($.fn.getUserRole() != undefined && $.fn.getUserRole().trim().toUpperCase() == "READER"){ 
        $('a[class = "navbar-brand"]').attr("data-target","")  
    }else{
        $('a[class = "navbar-brand"]').html("CREATE POST");
    }
    //Initial Load of id from Home or Direct url hit.
    var postId = localStorage.getItem('post');
    if(postId == undefined || postId == null || postId.trim() == ''){ //Direct URL Hit
        initLoadDirect();
    }else { //From HOME screen
       initLoadFromHome(postId);
    }
    // Initial post loading 
    function initLoadDirect(){
        jQuery.ajax({
            type: "GET",
            url: "/documentaryrest/rest/documentary/limit/1",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                result.data.forEach(element => {
                    localStorage.setItem('post',element.id);
                    postId=element.id; //Optional
                    initLoad(element)
                });
                
            }, error: function (response) {
                alert(response.responseJSON.message);
                if(response.status == 401){
	                $(location).attr('href', '/documentaryweb/connect-to-app');
	                localStorage.clear();
                }
            }
        });    
    }
    function initLoadFromHome(postId) {
         jQuery.ajax({
            type: "GET",
            url: "/documentaryrest/rest/documentary/" + postId,
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            success: function (result) {
                initLoad(result.data);
            }, error: function (response) {
                console.log(response);
                alert(response.responseJSON.message);
                $(location).attr('href', '/documentaryweb/connect-to-app');
                localStorage.clear();
            }
        });
    }
    function initLoad(data) {
        var postHeader = '<div class="container">'
            + '<div class="row">'
            + '<div class="col-lg-8 col-md-10 mx-auto">'
            + '<div class="post-heading">';
            
        
            postHeader += '<h1>'+data.title+'</h1>'
                        + '<span class="meta">- Posted by '
                        + '<a href="#">'+data.userId+'</a></span>';

            if($.fn.getUserRole().trim().toUpperCase() == 'WRITER'){
                postHeader += '<br><button class="btn btn-primary" id="postUpdate" name="postUpdate">Update</button>';
            }

            postHeader += '</div></div></div></div>';
        $('.masthead').html(postHeader);
        $.getScript( "resources/js/post.js");
        $('.description').html(data.description);
    }

    //Save Changes of post
    $("#create-post-form").submit(function(e){
    	debugger;
    	var isValid = true;
    	var postId = localStorage.getItem('post');
    	postId = postId==null?'':postId;
    	
    	/*Form To JSON*/
        var formData = {};
        var formArray = $(this).serializeArray();
        formArray.push({"name":"description","value":$('#description').Editor("getText")});
		formArray.push({"name":"id","value":postId});
		formArray.push({"name":"userId","value":$.fn.getUserId()});
		formArray.forEach( s => { formData[s.name] = s.value; } );
        
        if (!isValid) {
            e.preventDefault();
            return false;
        }
        else {
            jQuery.ajax({
                type: ($.fn.getPostUpdateFlag() == 'true')?"PUT":"POST",
                url: "/documentaryrest/rest/documentary/",
                contentType: "application/json;charset=UTF-8",
                dataType: "json",
                data:JSON.stringify(formData),
                
                success: function (result) {
                    console.log(result);                    
                    $('#create-post-form').trigger("reset");
                    $('#description').Editor("setText","");
                    if($.fn.getPostUpdateFlag() == 'true'){
                        initLoadFromHome(postId);
                    }
                    $("#postModal .close").click();
                    $.fn.setPostUpdateFlag('false');
                    
                }, error: function(response) {
                    // console.log(response);                        
                    if(response.responseJSON.status == 401){
                        alert(response.responseJSON.message);
                        $(location).attr('href', '/documentaryweb/connect-to-app');
                        localStorage.clear();
                    }else{
                        if(response.responseJSON.message == undefined){
                            alert("Create Post failed!");
                        }else{
                            alert(response.responseJSON.message);
                        }
                    }
                    $.fn.setPostUpdateFlag('false');
                }
            });
            
            e.preventDefault();
            return false;
        }
    });

    

});
/* Authentication Token */
$.fn.setToken = function (token) {
	localStorage.setItem('Authorization',token);
}
$.fn.getToken = function () {
	return localStorage.getItem('Authorization');
}

/* User Information */
$.fn.setUserId = function (userId) {
	localStorage.setItem('UserId',userId);
}
$.fn.getUserId = function(){
	return localStorage.getItem('UserId');
}
$.fn.setUserName = function (userName) {
	localStorage.setItem('UserName',userName);
}
$.fn.getUserName = function(){
	return localStorage.getItem('UserName');
}
$.fn.setUserRole = function (role) {
	localStorage.setItem('UserRole',role);
}
$.fn.getUserRole = function(){
	return localStorage.getItem('UserRole');
}

$.fn.checkPermission = function(role){
	return localStorage.getItem('UserRole') == role;
}

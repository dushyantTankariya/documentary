function include(file) {
    var script = document.createElement('script');
    script.src = file;
    script.type = 'text/javascript';
    script.defer = true;
    document.getElementsByTagName('head').item(0).appendChild(script);
}
$( document ).ready(function() {
	include('resources/routing/Auth.js');
	$("#register-form").submit(function(e){
		var isValid = true;
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		if (!isValid) {
			e.preventDefault();
			return false;
		}
		else {
			jQuery.ajax({
				type: "POST",
				url: "/documentaryrest/rest/user/register",
				contentType: "application/json;charset=UTF-8",
				dataType: "json",
				
				data:JSON.stringify(formData),
				
				success: function (result) {
					/* console.log(result);
					alert("Registered Successful."); */
					$('#register-form').trigger("reset");
					$('#login-form-link').trigger("click");
				}, error: function(response) {
					console.log(response);
					alert("Registeration failed!");
				}
			});
			e.preventDefault();
			return false;
		}
	});
	

	$("#login-form").submit(function(e){
		var isValid = true;
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		if (!isValid) {
			e.preventDefault();
			return false;
		}
		else {
			jQuery.ajax({
				type: "POST",
				url: "/documentaryrest/rest/user/login",
				contentType: "application/json;charset=UTF-8",
				dataType: "json",
				
				data:JSON.stringify(formData),
				
				success: function (result) {
					// console.log(result);
					// alert("Login Successful.");
					$.fn.setToken(result.data.token);
					$.fn.setUserId(result.data.id);
					$.fn.setUserName(result.data.firstname + ' ' + result.data.lastname);
					$.fn.setUserRole(result.data.role);
					if(result.data.role == 'ops'){
						$(location).attr('href', '/documentaryweb/dashboard');
					}else if(result.data.role == 'admin'){
						$(location).attr('href', '/documentaryweb/userMgmt');
					}else{
						$(location).attr('href', '/documentaryweb/documentary');
					}
				}, error: function(response) {
					console.log(response);
					alert("Login failed!");
				}
			});
			e.preventDefault();
			return false;
		}
	});
});
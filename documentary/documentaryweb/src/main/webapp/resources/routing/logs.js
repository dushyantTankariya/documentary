$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
	$.getScript( "resources/js/logger.js", function() {
		$.holdReady( false );
	});  
});
$(document).ready(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
        	/*if(!$.fn.checkPermission('ops')) {
        		window.history.go(-1)
        		return false;
        	}*/
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    init();    
}); 
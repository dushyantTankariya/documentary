$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
  $.holdReady( false );
});
$(document).ready(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/documentary/",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
            initLoad(result.data);
        }, error: function (response) {
            alert(response.responseJSON.message);
            if(response.status == 401){
	            $(location).attr('href', '/documentaryweb/connect-to-app');
	            localStorage.clear();
            }
        }
    });

    function initLoad(data) {
        var post = '<table id="dTable" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">';
        post += '<thead><tr> <th class="th-sm">TItle</th></tr></thead>';
        post += '<tbody>';
        data.forEach(element => {
            post += '<tr><td><a name="'+element.id+'" href="post" id= ><h2 class="post-title">' + element.title + '</h2></a></td></tr>';
        });
        post += '</tbody><tfoot><th>Title</th></tr></tfoot></table>';
        $('#listpost').html(post);
        $('a[href = "post"]').click(function (e) {
            localStorage.setItem('post', e.currentTarget.name);
        });
        $('#dTable').DataTable();
        
    }
});
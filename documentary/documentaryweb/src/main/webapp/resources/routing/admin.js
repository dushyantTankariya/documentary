$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
	$.getScript( "resources/js/adm.js", function() {
		$.holdReady( false );
	});  
});
$(document).ready(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    init();
}); 
$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
  $.holdReady( false );
});
$(document).ready(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/health/db",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
        	if(result.data.result >= 1){
        		$('#db').html('UP');
        		$('#db').removeClass('badge-danger');
        		$('#db').addClass('badge-success');        		
        	}else {
        		$('#db').html('DOWN');
        		$('#db').removeClass('badge-success');
        		$('#db').addClass('badge-danger');
        	}
        	$('#dbName').html(result.data.database);
        	$('#result').html(result.data.result);
        	$('#query').html(result.data.validationQuery);
        }, error: function (response) {
        	alert(response.responseJSON.message);
        	$('#db').html('DOWN');
        	$('#db').removeClass('badge-success');
    		$('#db').addClass('badge-danger');
        	if(response.status == 401){
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/health/diskSpace",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
        	if(result.data.threshold > 10){
        		$('#disk').html('UP');
        		$('#disk').removeClass('badge-danger');
        		$('#disk').addClass('badge-success');        		
        	}else {
        		$('#disk').html('DOWN');
        		$('#disk').removeClass('badge-success');
        		$('#disk').addClass('badge-danger');
        	}
        	$('#totalSpace').html(result.data.total.toFixed(2) + ' GB');
        	$('#freeSpace').html(result.data.free.toFixed(2) + ' GB');
        	$('#thresholdSpace').html(result.data.threshold.toFixed(2) + ' GB');
        }, error: function (response) {
        	alert(response.responseJSON.message);
        	$('#disk').html('DOWN');
    		$('#disk').removeClass('badge-success');
    		$('#disk').addClass('badge-danger');
        	if(response.status == 401){
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/health/application",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
        	if(result.status >= 200 && result.status < 299){
        		$('#application').html('UP');
        		$('#application').removeClass('badge-danger');
        		$('#application').addClass('badge-success');        		
        	}else {
        		$('#application').html('DOWN');
        		$('#application').removeClass('badge-success');
        		$('#application').addClass('badge-danger');
        	}
        }, error: function (response) {
        	$('#application').html('DOWN');
    		$('#application').removeClass('badge-success');
    		$('#application').addClass('badge-danger');
        	if(response.status == 401){
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
});    
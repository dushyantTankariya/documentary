$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
  $.holdReady( false );
});
$(document).ready(function () {
    $.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/dashboard/",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",
        success: function (result) {
        	diskSpaceAndMemory(result.data[0]);
        	threadInfo(result.data[1]);
        	systemInfo(result.data[2]);
    		
        }, error: function (response) {
            alert(response.responseJSON.message);
            if(response.status == 401){
	            $(location).attr('href', '/documentaryweb/connect-to-app');
	            localStorage.clear();
            }
        }
    });
    
    function diskSpaceAndMemory(data){
    	$('#diskSpan').text(data.diskSpace + '%');
		$('#diskDiv').attr('style','width: '+ data.diskSpace + '%');
		$('#diskDiv').attr('aria-valuenow', data.diskSpace + '%');
		
		$('#heapSpan').text(data.heap + '%');
		$('#heapDiv').attr('style','width: '+ data.heap + '%');
		$('#heapDiv').attr('aria-valuenow', data.heap + '%');
    }
    
    function threadInfo(data){
    	$('#activeCount').html(data.activeCount);
    	$('#daemonCount').html(data.daemonCount);
    	$('#totalStartedCount').html(data.totalStartedCount);
    }
    
    function systemInfo(data){
    	$('#osName').val(data.osName);
    	$('#osVersion').val(data.osVersion);
    	$('#osLoadAvg').val(data.loadAvg);
    	$('#processors').val(data.availableProcess);
    	$('#upTime').val(new Date(data.upTime).toISOString().slice(11,19));
    	$('#upTime').attr('title',new Date(data.upTime).toISOString().slice(11,19));
    	$('#startTime').val(new Date(data.startTime).toUTCString());
    	$('#startTime').attr('title',new Date(data.startTime).toUTCString());
    }
});
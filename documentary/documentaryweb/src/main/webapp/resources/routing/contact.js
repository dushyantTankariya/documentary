$.holdReady( true );
$.getScript( "resources/routing/Auth.js", function() {
	$.getScript( "resources/routing/util.js", function() {
        $.holdReady( false );
    });
});

$(document).ready(function () {
	$.ajaxSetup({
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', $.fn.getToken());
            xhr.setRequestHeader('UserId', $.fn.getUserId());
            xhr.setRequestHeader('UserRole', $.fn.getUserRole());
        }
    });
    //When page load -> Verify the user is authorized or not.
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/contact/isTokenValid",
        contentType: "application/json;charset=UTF-8",
        dataType: "json",

        error: function (response) {
        	alert(response.responseJSON.message);
            if(response.status == 401){
                $(location).attr('href', '/documentaryweb/connect-to-app');
                localStorage.clear();
            }
        }
    });    
	$('#contactForm').submit(function(e){
        var isValid = true;
        var formData = {};
        var formArray = $(this).serializeArray();
        
        formArray.push({"name":"userId","value":$.fn.getUserId()});
        formArray.forEach( s => { formData[s.name] = s.value; } );
        
        if (!isValid) {
            e.preventDefault();
            return false;
        }
        else {
            jQuery.ajax({
                type: "POST",
                url: "/documentaryrest/rest/contact/",
                contentType: "application/json;charset=UTF-8",
                dataType: "json",
                data:JSON.stringify(formData),
                success: function(result) {
                    console.log(result);
                    // alert("Request processed Successful.");
                    // Success message
                    $('#success').html("<div class='alert alert-success'>");
                    $('#success > .alert-success').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                        .append("</button>");
                    $('#success > .alert-success')
                        .append("<strong>Your message has been sent. </strong>");
                    $('#success > .alert-success')
                        .append('</div>');
                    //clear all fields
                    $('#contactForm').trigger("reset");
                },
                error: function(response) {
                    if(response.responseJSON.status == 401){
                        alert(response.responseJSON.message);
                        $(location).attr('href', '/documentaryweb/connect-to-app');
                        localStorage.clear();
                    }else{
                        if(response.responseJSON.message == undefined){
                            alert("Request process failed!");
                        }else{
                            alert(response.responseJSON.message);
                        }
                        // Fail message
                        $('#success').html("<div class='alert alert-danger'>");
                        $('#success > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
                            .append("</button>");
                        $('#success > .alert-danger').append($("<strong>").text("Sorry " + firstName + ", it seems that my mail server is not responding. Please try again later!"));
                        $('#success > .alert-danger').append('</div>');
                        //clear all fields
                        $('#contactForm').trigger("reset");
                    }
                  
                },
                complete: function() {
                    setTimeout(function() {
                        $(this).prop("disabled", false); // Re-enable submit button when AJAX call is complete
                    }, 1000);
                }
            });
            e.preventDefault();
            return false;
        }
    });
});
var moduleInfo;
$.fn.setModuleInfo = function(module){
	this.moduleInfo = module;
}
$.fn.getModuleInfo = function(){
	return this.moduleInfo;
}
$.fn.buildTable = function(result) {
	var data = '';
	$.each(result.data, function(index, value) {
		data += '<tr><input id="key-' + index + '" type="hidden" disabled="disabled" value="' + value.id + '"/>';		
		data += '<td><input id="name' + index + '" type="text" disabled="disabled" value="' + value.name + '"/></td>';

		/*Module Select picker*/
		data += '<td><select id="module-' + index + '" disabled="disabled" class="selectpicker" data-live-search="true">';
		data += '<option value="' + value.module.split("->")[0] + '" selected>' + value.module.split("->")[1] + '</option>';
		var valArr = "";
		$.each($.fn.getModuleInfo().data, function(idx,val){
			if(!(val.name === value.module.split("->")[1]) && valArr.search(val.name)<0){
				data += '<option value="' + val.id + '">' + val.name + '</option>';
				valArr = valArr.concat(val.name);
			}
		});
		
		data += '</select></td>';
		
		data += '<td><input id="createdBy' + index + '" type="text" disabled="disabled" value="' + value.createdBy + '"/></td>';
		data += '<td><input id="createdDate' + index + '" type="text" disabled="disabled" value="' + value.createdDate + '"/></td>';
		data += '<td><input id="updatedBy' + index + '" type="text" disabled="disabled" value="' + (value.updatedBy==null ? '' : value.updatedBy) + '"/></td>';
		data += '<td><input id="updatedDate' + index + '" type="text" disabled="disabled" value="' + (value.updatedDate==null ? '' : value.updatedDate) + '"/></td>';
		/*Action*/
		data += '<td class="text-nowrap">'
			  
			  /*Save*/
			  + '<button id="save-' + index + '" class="btn btn-sm btn-success" style="display:none">'
			  	+ '<span class="fa fa-save"></span>'
			  + '</button>'
			  
			  /*Edit*/
			  + '<button id="edit-' + index + '" class="btn btn-sm btn-info ml-1">'
				+ '<span class="fa fa-edit"></span>'
			  + '</button>'
			  
			  /*Delete*/
			  + '<button id="delete-' + index + '" class="btn btn-sm btn-danger ml-1">'
			  	+ '<span class="fa fa-trash"></span>'
			  + '</button>';
	
		data += '</td></tr>';
	});
	return data;
}
$.fn.upgradeToDataTable = function(data){
	
	$('table').dataTable({
		"sScrollX" : "100%",
		"bScrollCollapse" : true,
		"bJQueryUI" : true,
		"bPaginate": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bProcessing": true,
		"aoColumnDefs" : [ 
			{"aTargets" : [ 0 ], "bSortable" : true}, 
			{"aTargets" : [ '_all' ], "bSortable" : false}
		],
		"aaSorting" : [ [ 0, 'asc' ] ]
	});
	
	setTimeout(function() {
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust();
	}, 5);
}

$.fn.registerModuleEvent = function(){
	var editable = 1;
	/*Edit*/
	$("table").on("click", "button[id^=edit-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		editable++;		
		if(editable%2==0){
			$('#name' + id).removeAttr("disabled");
			$('#module-' + id).removeAttr("disabled");
			$('button[data-id=module-' + id + ']').removeClass("disabled");
			$('#save-' + id).removeAttr("style");
		}else{
			$('#name' + id).attr("disabled","disabled");
			$('#module-' + id).attr("disabled","disabled");
			$('button[data-id=module-' + id + ']').addClass("disabled");
			$('#save-' + id).attr("style","display:none");
		}
		
	});
	/*Save*/
	$('table').on("click", "button[id^=save-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		var formData = {			
			id : $('#key-' + id)[0].value,
			name : $('#name' + id)[0].value,
			moduleId : $('#module-' + id)[0].value
		};
		jQuery.ajax({
            type: "PUT",
            url: "/documentaryrest/rest/submodule/",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data:JSON.stringify(formData),
            
            success: function (result) {
                //console.log(result);
            	//$('button[id^=edit-' + id + ']').click();
            	$.fn.initSubModule();
            }, error: function(response) {
                console.log(response);
            }
        });
	});
	/*Delete*/
	$("table").on("click", "button[id^=delete-]", function(data, handler){
		if (confirm("Are you sure?")) {
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			jQuery.ajax({
	            type: "DELETE",
	            url: "/documentaryrest/rest/submodule/" + $('#key-' + id)[0].value,
	            contentType: "application/json;charset=UTF-8",
	            dataType: "json",
	            success: function (result) {
	                //console.log(result);
	            	$.fn.initSubModule();
	            }, error: function(response) {
	                console.log(response);
	            }
	        });
		}
	});
	
	
	/*ModuleForm*/
	$('#subModuleForm').submit(function(e) {
		/*Prevent default form submit*/
		e.preventDefault();
		
		/*Form to JSON*/
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		
		/*Save Record*/
		jQuery.ajax({
			type: "POST",
			url: "/documentaryrest/rest/submodule/",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			data:JSON.stringify(formData),
			success: function (result) {
				$('#subModuleForm').trigger("reset");
				$('#closeSubModuleModel').trigger("click");
				$.fn.initSubModule();
			}, error: function(response) {
				console.log(response);
				alert("failed!");
			}
		});
		
	});
	
	$('table').on("change", "input[text]", function(){
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust().draw();
	});
	
	$('table').on( 'page.dt', function () {
		setTimeout(function() {
			$(".selectpicker").selectpicker('refresh');
			$('table').DataTable().columns.adjust();
		}, 5);
	});
	
	
	 
}
/*Initiate Module*/
$.fn.initModule = function(){
	var valArr = "";
	$.each($.fn.getModuleInfo().data, function(idx,val){
		if(valArr.search(val.name)<0){
			$('#moduleId').append('<option value="' + val.id + '">' + val.name + '</option>');
			valArr = valArr.concat(val.name,',');
		}
	});
	$(".selectpicker").selectpicker('refresh');
}
/*Initiate Sub Module*/
$.fn.initSubModule = function(){
	/*Fetch Module Information*/
	jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/module/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
        	$.fn.setModuleInfo(result);
        	/*Fetch Sub Module Information*/
		    jQuery.ajax({
		        type: "GET",
		        url: "/documentaryrest/rest/submodule/",
		        contentType: "application/json;charset=UTF-8",
		        success: function (result) {
		        	var dataTableFlag = $.fn.dataTable.isDataTable( 'table' ); 
		        	if ( dataTableFlag ) {
		        		$('table').DataTable().destroy();
		        	}
		        	$('tbody').empty();
		        	$('tbody').html($.fn.buildTable(result));
		        	
		        	$.fn.upgradeToDataTable();
		        	
		        	if (! dataTableFlag ) {	
		        		$.fn.registerModuleEvent();
		        		$.fn.initModule();
		        	}
		        }, error: function (response) {
		        	if(response.status == 401){
		        		alert(response.responseJSON.message);
		        		$(location).attr('href', '/documentaryweb/connect-to-app');
		        		localStorage.clear();
		        	}
		        }
		    });
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}

var screenInfo;
$.fn.setScreenInfo = function(d){
	this.screenInfo = d;
}
$.fn.getScreenInfo = function(){
	return this.screenInfo;
}
$.fn.buildTable = function(result) {
	var data = '';
	$.each(result.data, function(index, value) {
		data += '<tr><input id="key-' + index + '" type="hidden" disabled="disabled" value="' + value.id + '"/>';
		data += '<input id="rolecode' + index + '" type="hidden" disabled="disabled" value="' + value.roleCode + '"/>';
		data += '<td><input id="name' + index + '" type="text" disabled="disabled" value="' + value.name + '"/></td>';
		/*Screen Select picker*/
		data += '<td><select id="screen-' + index + '" disabled="disabled" class="selectpicker" data-live-search="true">';
		data += '<option value="' + value.screen.split("->")[0] + '" selected>' + value.screen.split("->")[1] + '</option>';
		$.each($.fn.getScreenInfo().data, function(idx,val){
			if(!(val.name === value.screen.split("->")[1]) ){
				data += '<option value="' + val.id + '">' + val.name + '</option>';
			}
		});
		data += '</select></td>';
		
		data += '<td><input id="submodule-' + index + '" type="text" disabled="disabled" value="' + value.screen.split("->")[3] + '"/></td>';
		data += '<td><input id="module-' + index + '" type="text" disabled="disabled" value="' + value.screen.split("->")[5] + '"/></td>';

		data += '<td style="display:flex;width:max-content;">';
			data += '<label id="lblwriter-' + index + '" ' + ((value.access == 'W') ? 'class="lblWriter"' : '') + ' >';
				data += '<input type="radio" id="writer-' + index + '" name="access' + index + '" ' + ((value.access == 'W') ? 'checked="checked"' : '')  + ' value="W" disabled="disabled"> Write'
			data += '</label>';
			data += '<label id="lblreader-' + index + '" ' + ((value.access == 'R') ? 'class="lblReader"' : '') + ' >';
				data += '<input type="radio" id="reader-' + index + '" name="access' + index + '" ' + ((value.access == 'R') ? 'checked="checked"' : '' ) + ' value="R" disabled="disabled" > Read';
			data += '</label>';
		data += '</td>'
		
		/*Action*/
		data += '<td class="text-nowrap">'
			  
			  /*Save*/
			  + '<button id="save-' + index + '" class="btn btn-sm btn-success" style="display:none">'
			  	+ '<span class="fa fa-save"></span>'
			  + '</button>'
			  
			  /*Edit*/
			  + '<button id="edit-' + index + '" class="btn btn-sm btn-info ml-1">'
				+ '<span class="fa fa-edit"></span>'
			  + '</button>'
			  
			  /*Delete*/
			  + '<button id="delete-' + index + '" class="btn btn-sm btn-danger ml-1">'
			  	+ '<span class="fa fa-trash"></span>'
			  + '</button>';
	
		data += '</td></tr>';
	});
	return data;
}
$.fn.upgradeToDataTable = function(data){
	
	$('table').dataTable({
		"sScrollX" : "100%",
		"bScrollCollapse" : true,
		"bJQueryUI" : true,
		"bPaginate": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bProcessing": true,
		"aoColumnDefs" : [ 
			{"aTargets" : [ 0 ], "bSortable" : true}, 
			{"aTargets" : [ '_all' ], "bSortable" : false}
		],
		"aaSorting" : [ [ 0, 'asc' ] ]
	});
	
	setTimeout(function() {
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust();
	}, 5);
}

$.fn.registerScreenEvent = function(){
	var editable = 1;
	/*Edit*/
	$("table").on("click", "button[id^=edit-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		editable++;		
		if(editable%2==0){
			$('#name' + id).removeAttr("disabled");
			$('#screen-' + id).removeAttr("disabled");
			$('button[data-id=screen-' + id + ']').removeClass("disabled");
			$('#reader-' + id).removeAttr("disabled");
			$('#writer-' + id).removeAttr("disabled");
			$('#save-' + id).removeAttr("style");
		}else{
			$('#name' + id).attr("disabled","disabled");
			$('#screen-' + id).attr("disabled","disabled");
			$('button[data-id=screen-' + id + ']').addClass("disabled");
			$('#reader-' + id).removeAttr("disabled","disabled");
			$('#writer-' + id).removeAttr("disabled","disabled");
			$('#save-' + id).attr("style","display:none");
		}
	});
	/*Save*/
	$('table').on("click", "button[id^=save-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		var formData = {			
			id : $('#key-' + id)[0].value,
			roleCode: $('#rolecode' + id)[0].value,
			name : $('#name' + id)[0].value,
			screen: $('#screen-' + id)[0].value,
			access: $('[name="access' + id + '"]')[0].checked ? $('[name="access' + id + '"]')[0].value :$('[name="access' + id + '"]')[1].value
		};
		jQuery.ajax({
            type: "PUT",
            url: "/documentaryrest/rest/role/",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data:JSON.stringify(formData),
            
            success: function (result) {
                //console.log(result);
            	$.fn.initRole();
            }, error: function(response) {
                console.log(response);
            }
        });
	});
	/*Delete*/
	$("table").on("click", "button[id^=delete-]", function(data, handler){
		if (confirm("Are you sure?")) {
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			jQuery.ajax({
	            type: "DELETE",
	            url: "/documentaryrest/rest/role/" + $('#key-' + id)[0].value,
	            contentType: "application/json;charset=UTF-8",
	            dataType: "json",
	            success: function (result) {
	                //console.log(result);
	            	$.fn.initRole();
	            }, error: function(response) {
	                console.log(response);
	            }
	        });
		}
	});
	$("table").on("change", "input[id^=writer-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		if($('#writer-' + id + ':checked').value = 'on'){
			$('#lblwriter-' + id).addClass("lblWriter");
			$('#lblreader-' + id).removeClass("lblReader");
		}
	});
	$("table").on("change", "input[id^=reader-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		if($('#reader-' + id + ':checked').value = 'on'){
			$('#lblreader-' + id).addClass("lblReader");
			$('#lblwriter-' + id).removeClass("lblWriter");
		}
	});
	/*Table Submodule*/
	$("table").on("change", "select[id^=screen-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		$("#submodule-" + id).val ( 
				$.fn.getScreenInfo()
					.data
					.filter(dt => dt.id == data.currentTarget.value)
					.map( m=> m.submodule.split("->")[1])
					.join(",")
		);
		$("#module-" + id).val ( 
				$.fn.getScreenInfo()
					.data
					.filter(dt => dt.id == data.currentTarget.value)
					.map( m=> m.submodule.split("->")[3])
					.join(",")
		);
	});
	
	/*Form ADd New Role*/
	$("#screenId").on("change", function(data, handler){
		$("#submoduleId").val ( 
			$.fn.getScreenInfo()
				.data
				.filter(dt => dt.id == data.currentTarget.value)
				.map(m=> m.submodule.split("->")[1])
				.join(",") 
		);
		$("#moduleId").val ( 
				$.fn.getScreenInfo()
					.data
					.filter(dt => dt.id == data.currentTarget.value)
					.map(m=> m.submodule.split("->")[3])
					.join(",") 
		);
	});
	$("#writerId").on("change", function(data, handler){
		$('#lblwriterId').addClass("lblWriter");
		$('#lblreaderId').removeClass("lblReader");
	});
	$("#readerId").on("change", function(data, handler){
		$('#lblreaderId').addClass("lblReader");
		$('#lblwriterId').removeClass("lblWriter");
	});
	
	/*RoleFOrm*/
	$('#roleForm').submit(function(e) {
		/*Prevent default form submit*/
		e.preventDefault();
		
		/*Form to JSON*/
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		
		/*Save Record*/
		jQuery.ajax({
			type: "POST",
			url: "/documentaryrest/rest/role/",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			data:JSON.stringify(formData),
			success: function (result) {
				$('#roleForm').trigger("reset");
				$('#closeRoleModel').trigger("click");
				$.fn.initRole();
			}, error: function(response) {
				console.log(response);
				alert("failed!");
			}
		});
		
	});
	
	$('table').on("change", "input[text]", function(){
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust().draw();
	});
	
	$('table').on( 'page.dt', function () {
		setTimeout(function() {
			$(".selectpicker").selectpicker('refresh');
			$('table').DataTable().columns.adjust();
		}, 5);
	});
	 
}
/*Initiate Screen*/
$.fn.initScreen = function(){
	$.each($.fn.getScreenInfo().data, function(idx,val){
		$('#screenId').append('<option value="' + val.id + '">' + val.name + '</option>');
	});
	$("#submoduleId").val ( 
		$.fn.getScreenInfo()
			.data
			.filter(dt => dt.id == $('#screenId').val())
			.map(m=> m.submodule.split("->")[1])
			.join(",") 
	);
	$("#moduleId").val ( 
			$.fn.getScreenInfo()
				.data
				.filter(dt => dt.id == $('#screenId').val())
				.map(m=> m.submodule.split("->")[3])
				.join(",") 
	);
	$(".selectpicker").selectpicker('refresh');
}
/*Initiate Screen*/
$.fn.initRole = function(){
	/*Fetch Module Information*/
	jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/screen/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
        	$.fn.setScreenInfo(result);
        	/*Fetch Sub Module Information*/
		    jQuery.ajax({
		        type: "GET",
		        url: "/documentaryrest/rest/role/",
		        contentType: "application/json;charset=UTF-8",
		        success: function (result) {
		        	var dataTableFlag = $.fn.dataTable.isDataTable( 'table' ); 
		        	if ( dataTableFlag ) {
		        		$('table').DataTable().destroy();
		        	}
		        	$('tbody').empty();
		        	$('tbody').html($.fn.buildTable(result));
		        	
		        	$.fn.upgradeToDataTable();
		        	
		        	if (! dataTableFlag ) {	
		        		$.fn.registerScreenEvent();
		        		//InitScreen
		        		$.fn.initScreen();
		        	}
		        }, error: function (response) {
		        	if(response.status == 401){
		        		alert(response.responseJSON.message);
		        		$(location).attr('href', '/documentaryweb/connect-to-app');
		        		localStorage.clear();
		        	}
		        }
		    });
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}

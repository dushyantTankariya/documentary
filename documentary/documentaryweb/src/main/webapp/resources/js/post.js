
$(document).ready(function () {
        $("#postUpdate").click(function () { 
            // alert("button");
            $('a[class = "navbar-brand"]').click();
            $('#title').val($('.post-heading h1').html());
            $('#description').Editor("setText",$('.description').html());
            $.fn.setPostUpdateFlag(true);
        });
        $("#postModal .close").click(function () {
        	$('#create-post-form').trigger("reset");
            $('#description').Editor("setText","");
        	$.fn.setPostUpdateFlag(false);
        	$('#title').val("");
        });
});
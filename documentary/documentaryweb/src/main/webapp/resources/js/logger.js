function init(){
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/logger/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
			$('tbody').html(initLoad(result));
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}
function initLoad(result){
	var data;
	$.each(result.data, function(index, value) {    		
		data += '<tr><td>' + index + '</td><td>';
		
		switch(value){
    		case 'TRACE': 
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
				
    		case 'DEBUG': 
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
				
    		case 'INFO': 
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
				
    		case 'WARN': 
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
			
    		case 'ERROR': 
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
				
			case 'OFF': 
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'TRACE\')" >TRACE</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'DEBUG\')" >DEBUG</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'INFO\')" >INFO</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'WARN\')" >WARN</button>';
				data += '<button class="border btn btn-sm btn-light" onclick= "setLogLevel(\'' + index + '\', \'ERROR\')" >ERROR</button>';
				data += '<button class="border btn btn-sm btn-success" onclick= "setLogLevel(\'' + index + '\', \'OFF\')" >OFF</button>';
				break;
			
		}
		data += '</td></tr>';
	});
	return data;
}
function setLogLevel(clazz, level) {
	jQuery.ajax({
		type : "POST",
		url : "/documentaryrest/rest/logger/" + level,
		contentType : "application/json;charset=UTF-8",
		dataType : "text",
		data : "{packageName:" + clazz + "}",
		success : function(result) {
			init();
		},
		error : function(response) {
			if (response.status == 401) {
				$(location).attr('href', '/documentaryweb/connect-to-app');
				localStorage.clear();
			}
		}
	});
}

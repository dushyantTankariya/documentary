$(function() {

    $('#login-form-link').click(function(e) {
		$("#login-form").delay(100).fadeIn(100);
 		$("#register-form").fadeOut(100);
		$('#register-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#register-form-link').click(function(e) {
		$("#register-form").delay(100).fadeIn(100);
 		$("#login-form").fadeOut(100);
		$('#login-form-link').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});
	$('#writer').change(function(e){
		if($('#writer:checked').value = 'on'){
			$('#lblwriter').addClass("label label-info");
			$('#lblreader').removeClass("label label-info");
		}
	});
	$('#reader').change(function(e){
		if($('#reader:checked').value = 'on'){
			$('#lblreader').addClass("label label-info");
			$('#lblwriter').removeClass("label label-info");
		}
	});
	
});

var subModuleInfo;
$.fn.setSubModuleInfo = function(d){
	this.subModuleInfo = d;
}
$.fn.getSubModuleInfo = function(){
	return this.subModuleInfo;
}
$.fn.buildTable = function(result) {
	var data = '';
	$.each(result.data, function(index, value) {
		data += '<tr><input id="key-' + index + '" type="hidden" disabled="disabled" value="' + value.id + '"/>';		
		data += '<td><input id="name' + index + '" type="text" disabled="disabled" value="' + value.name + '"/></td>';
		data += '<td><input id="version' + index + '" type="text" disabled="disabled" value="' + value.version + '"/></td>';

		/*Module Select picker*/
		data += '<td><select id="submodule-' + index + '" disabled="disabled" class="selectpicker" data-live-search="true">';
		data += '<option value="' + value.submodule.split("->")[0] + '" selected>' + value.submodule.split("->")[1] + '</option>';
		/*var valArr = "";*/
		$.each($.fn.getSubModuleInfo().data, function(idx,val){
			if(!(val.name === value.submodule.split("->")[1]) /*&& valArr.search(val.name)<0*/){
				data += '<option value="' + val.id + '">' + val.name + '</option>';
				/*valArr = valArr.concat(val.name);*/
			}
		});
		
		data += '</select></td>';
		
		data += '<td><input id="module-' + index + '" type="text" disabled="disabled" value="' + value.submodule.split("->")[3] + '"/></td>';
		
		data += '<td><input id="createdBy' + index + '" type="text" disabled="disabled" value="' + value.createdBy + '"/></td>';
		data += '<td><input id="createdDate' + index + '" type="text" disabled="disabled" value="' + value.createdDate + '"/></td>';
		data += '<td><input id="updatedBy' + index + '" type="text" disabled="disabled" value="' + (value.updatedBy==null ? '' : value.updatedBy) + '"/></td>';
		data += '<td><input id="updatedDate' + index + '" type="text" disabled="disabled" value="' + (value.updatedDate==null ? '' : value.updatedDate) + '"/></td>';
		/*Action*/
		data += '<td class="text-nowrap">'
			  
			  /*Save*/
			  + '<button id="save-' + index + '" class="btn btn-sm btn-success" style="display:none">'
			  	+ '<span class="fa fa-save"></span>'
			  + '</button>'
			  
			  /*Edit*/
			  + '<button id="edit-' + index + '" class="btn btn-sm btn-info ml-1">'
				+ '<span class="fa fa-edit"></span>'
			  + '</button>'
			  
			  /*Delete*/
			  + '<button id="delete-' + index + '" class="btn btn-sm btn-danger ml-1">'
			  	+ '<span class="fa fa-trash"></span>'
			  + '</button>';
	
		data += '</td></tr>';
	});
	return data;
}
$.fn.upgradeToDataTable = function(data){
	
	$('table').dataTable({
		"sScrollX" : "100%",
		"bScrollCollapse" : true,
		"bJQueryUI" : true,
		"bPaginate": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bProcessing": true,
		"aoColumnDefs" : [ 
			{"aTargets" : [ 0 ], "bSortable" : true}, 
			{"aTargets" : [ '_all' ], "bSortable" : false}
		],
		"aaSorting" : [ [ 0, 'asc' ] ]
	});
	
	setTimeout(function() {
		
		
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust();
	}, 5);
}

$.fn.registerScreenEvent = function(){
	var editable = 1;
	/*Edit*/
	$("table").on("click", "button[id^=edit-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		editable++;		
		if(editable%2==0){
			$('#name' + id).removeAttr("disabled");
			$('#version' + id).removeAttr("disabled");
			$('#submodule-' + id).removeAttr("disabled");
			$('button[data-id=submodule-' + id + ']').removeClass("disabled");
			$('#save-' + id).removeAttr("style");
		}else{
			$('#name' + id).attr("disabled","disabled");
			$('#version' + id).attr("disabled","disabled");
			$('#submodule-' + id).attr("disabled","disabled");
			$('button[data-id=submodule-' + id + ']').addClass("disabled");
			$('#save-' + id).attr("style","display:none");
		}
		
	});
	/*Save*/
	$('table').on("click", "button[id^=save-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		var formData = {			
			id : $('#key-' + id)[0].value,
			name : $('#name' + id)[0].value,
			version: $('#version' + id)[0].value,
			submoduleId : $('#submodule-' + id)[0].value
		};
		jQuery.ajax({
            type: "PUT",
            url: "/documentaryrest/rest/screen/",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data:JSON.stringify(formData),
            
            success: function (result) {
                //console.log(result);
            	$.fn.initScreen();
            }, error: function(response) {
                console.log(response);
            }
        });
	});
	/*Delete*/
	$("table").on("click", "button[id^=delete-]", function(data, handler){
		if (confirm("Are you sure?")) {
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			jQuery.ajax({
	            type: "DELETE",
	            url: "/documentaryrest/rest/screen/" + $('#key-' + id)[0].value,
	            contentType: "application/json;charset=UTF-8",
	            dataType: "json",
	            success: function (result) {
	                //console.log(result);
	            	$.fn.initScreen();
	            }, error: function(response) {
	                console.log(response);
	            }
	        });
		}
	});
	
	/*Table Submodule*/
	$("table").on("change", "select[id^=submodule-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		$("#module-" + id).val ( 
				$.fn.getSubModuleInfo()
					.data
					.filter(dt => dt.id == data.currentTarget.value)
					.map( m=> m.module.split("->")[1])
			);
	});
	
	/*Form Submodule*/
	$("#submoduleId").on("change", function(data, handler){
		$("#moduleId").val ( 
			$.fn.getSubModuleInfo()
				.data
				.filter(dt => dt.id == data.currentTarget.value)
				.map(m=> m.module.split("->")[1])
				.join(",") 
		);
	});
	
	/*ModuleForm*/
	$('#screenForm').submit(function(e) {
		/*Prevent default form submit*/
		e.preventDefault();
		
		/*Form to JSON*/
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		
		/*Save Record*/
		jQuery.ajax({
			type: "POST",
			url: "/documentaryrest/rest/screen/",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			data:JSON.stringify(formData),
			success: function (result) {
				$('#screenForm').trigger("reset");
				$('#closeScreenModel').trigger("click");
				$.fn.initScreen();
			}, error: function(response) {
				console.log(response);
				alert("failed!");
			}
		});
		
	});
	
	$('table').on("change", "input[text]", function(){
		$(".selectpicker").selectpicker('refresh');
		$('table').DataTable().columns.adjust().draw();
	});
	
	$('table').on( 'page.dt', function () {
		setTimeout(function() {
			$(".selectpicker").selectpicker('refresh');
			$('table').DataTable().columns.adjust();
		}, 5);
	});
	 
}
/*Initiate Sub Module*/
$.fn.initSubModule = function(){
	$.each($.fn.getSubModuleInfo().data, function(idx,val){
		$('#submoduleId').append('<option value="' + val.id + '">' + val.name + '</option>');
	});
	$(".selectpicker").selectpicker('refresh');
	$("#moduleId").val ( 
		$.fn.getSubModuleInfo()
			.data
			.filter(dt => dt.id == $("#submoduleId").val())
			.map(m=> m.module.split("->")[1])
			.join(",") 
	);
}
/*Initiate Screen*/
$.fn.initScreen = function(){
	/*Fetch Module Information*/
	jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/submodule/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
        	$.fn.setSubModuleInfo(result);
        	/*Fetch Sub Module Information*/
		    jQuery.ajax({
		        type: "GET",
		        url: "/documentaryrest/rest/screen/",
		        contentType: "application/json;charset=UTF-8",
		        success: function (result) {
		        	var dataTableFlag = $.fn.dataTable.isDataTable( 'table' ); 
		        	if ( dataTableFlag ) {
		        		$('table').DataTable().destroy();
		        	}
		        	$('tbody').empty();
		        	$('tbody').html($.fn.buildTable(result));
		        	
		        	$.fn.upgradeToDataTable();
		        	
		        	if (! dataTableFlag ) {	
		        		$.fn.registerScreenEvent();
		        		//InitSubModule
		        		$.fn.initSubModule();
		        	}
		        }, error: function (response) {
		        	if(response.status == 401){
		        		alert(response.responseJSON.message);
		        		$(location).attr('href', '/documentaryweb/connect-to-app');
		        		localStorage.clear();
		        	}
		        }
		    });
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}

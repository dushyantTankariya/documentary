
$.fn.buildTable = function(result) {
	var data = '';
	$.each(result.data, function(index, value) {
		data += '<tr><input id="key-' + index + '" type="hidden" disabled="disabled" value="' + value.id + '"/>';		
		data += '<td><input id="name' + index + '" type="text" disabled="disabled" value="' + value.name + '"/></td>';
		data += '<td><input id="createdBy' + index + '" type="text" disabled="disabled" value="' + value.createdBy + '"/></td>';
		data += '<td><input id="createdDate' + index + '" type="text" disabled="disabled" value="' + value.createdDate + '"/></td>';
		data += '<td><input id="updatedBy' + index + '" type="text" disabled="disabled" value="' + (value.updatedBy==null ? '' : value.updatedBy) + '"/></td>';
		data += '<td><input id="updatedDate' + index + '" type="text" disabled="disabled" value="' + (value.updatedDate==null ? '' : value.updatedDate) + '"/></td>';
		/*Action*/
		data += '<td class="text-nowrap">'
			  
			  /*Save*/
			  + '<button id="save-' + index + '" class="btn btn-sm btn-success" style="display:none">'
			  	+ '<span class="fa fa-save"></span>'
			  + '</button>'
			  
			  /*Edit*/
			  + '<button id="edit-' + index + '" class="btn btn-sm btn-info ml-1">'
				+ '<span class="fa fa-edit"></span>'
			  + '</button>'
			  
			  /*Delete*/
			  + '<button id="delete-' + index + '" class="btn btn-sm btn-danger ml-1">'
			  	+ '<span class="fa fa-trash"></span>'
			  + '</button>';
	
		data += '</td></tr>';
	});
	return data;
}
$.fn.upgradeToDataTable = function(data){
	
	
	if ( $.fn.dataTable.isDataTable( 'table' ) ) {
		$('table').DataTable().destroy();
	}
	
	$('table').dataTable({
		"sScrollX" : "100%",
		"bScrollCollapse" : true,
		"bJQueryUI" : true,
		"bPaginate": true,
		"bInfo": true,
		"bAutoWidth": true,
        "bProcessing": true,
		"aoColumnDefs" : [ 
			{"aTargets" : [ 0 ], "bSortable" : true}, 
			{"aTargets" : [ '_all' ], "bSortable" : false}
		],
		"aaSorting" : [ [ 0, 'asc' ] ]
	});
	
	
}

$.fn.registerModuleEvent = function(){
	var editable = 1;
	/*Edit*/
	$("table").on("click", "button[id^=edit-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		editable++;		
		if(editable%2==0){
			$('#name' + id).removeAttr("disabled");
			$('#save-' + id).removeAttr("style");
		}else{
			$('#name' + id).attr("disabled","disabled");
			$('#save-' + id).attr("style","display:none");
		}
		
	});
	/*Save*/
	$('table').on("click", "button[id^=save-]", function(data, handler){
		var id = data.currentTarget.getAttribute('id').split("-")[1];
		var formData = {			
			id : $('#key-' + id)[0].value,
			name : $('#name' + id)[0].value,
		};
		jQuery.ajax({
            type: "PUT",
            url: "/documentaryrest/rest/module/",
            contentType: "application/json;charset=UTF-8",
            dataType: "json",
            data:JSON.stringify(formData),
            
            success: function (result) {
                //console.log(result);
            	$.fn.initModule();
            }, error: function(response) {
                console.log(response);
            }
        });
	});
	/*Delete*/
	$("table").on("click", "button[id^=delete-]", function(data, handler){
		if (confirm("Are you sure?")) {
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			jQuery.ajax({
	            type: "DELETE",
	            url: "/documentaryrest/rest/module/" + $('#key-' + id)[0].value,
	            contentType: "application/json;charset=UTF-8",
	            dataType: "json",
	            success: function (result) {
	                //console.log(result);
	            	$.fn.initModule();
	            }, error: function(response) {
	                console.log(response);
	            }
	        });
		}
	});
	
	
	/*ModuleForm*/
	$('#moduleForm').submit(function(e) {
		/*Prevent default form submit*/
		e.preventDefault();
		
		/*Form to JSON*/
		var formData = {};
		$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
		
		/*Save Record*/
		jQuery.ajax({
			type: "POST",
			url: "/documentaryrest/rest/module/",
			contentType: "application/json;charset=UTF-8",
			dataType: "json",
			data:JSON.stringify(formData),
			success: function (result) {
				$('#moduleForm').trigger("reset");
				$('#closeModuleModel').trigger("click");
				$.fn.initModule();
			}, error: function(response) {
				console.log(response);
				alert("failed!");
			}
		});
		
	});
	
	$('table').on( 'page.dt', function () {
		setTimeout(function() {
			$('table').DataTable().columns.adjust();
		}, 5);
	});
	 
}
$.fn.initModule = function(){
	/*Fetch Module Information*/
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/module/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
        	$('tbody').empty();
        	$('tbody').html($.fn.buildTable(result));
        	if (! $.fn.dataTable.isDataTable( 'table' ) ) {
        		$.fn.upgradeToDataTable();
        		$.fn.registerModuleEvent();
        	}
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}

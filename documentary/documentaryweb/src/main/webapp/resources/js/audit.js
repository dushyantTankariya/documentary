function init(){
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/audit/" + $('#start').val() + " 00:00:00/" + $('#end').val() + " 23:59:59" ,
        contentType: "application/json;charset=UTF-8",
        success: function (result) {
			$('tbody').html(initLoad(result));
			$('#start').attr('max',$('#end').val());
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}
function initLoad(result){
	var data;
	$.each(result.data, function(index, value) {    		
		data += '<tr><td>' + value.username + '</td>';
		data += '<td>' + value.state + '</td>';
		data += '<td>' + new Date(value.statetimestamp).toGMTString() + '</td>'
		data += '</td></tr>';
	});
	return data;
}
$('input[type="date"]').change(function() {
	$('#start').attr('max',$('#end').val());
	init();
});
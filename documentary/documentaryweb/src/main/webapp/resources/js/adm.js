var screenInfo;
function setScreenInfo(scr){
	this.screenInfo = scr;
}
function getScreenInfo(){
	return this.screenInfo;
}
function init(){
	var roleInfo;
	/*Fetch Role Information*/
    jQuery.ajax({
        type: "GET",
        url: "/documentaryrest/rest/usrrole/",
        contentType: "application/json;charset=UTF-8",
        success: function (result) {        	
        	roleInfo = result;
        	 /*Fetch User Infomration*/
            jQuery.ajax({
                type: "GET",
                url: "/documentaryrest/rest/usrmgmt/",
                contentType: "application/json;charset=UTF-8",
                success: function (result) {
                	
                	var dataTableFlag = false;
                	if ( $.fn.dataTable.isDataTable( 'table' ) ) {
                		$('table').DataTable().destroy();
                		dataTableFlag= true;
                	}
                	setScreenInfo(roleInfo);
                	$('tbody').empty();
                	$('tbody').html(initLoad(roleInfo, result));
                	loadComplete(dataTableFlag);
                }, error: function (response) {
                	if(response.status == 401){
                		alert(response.responseJSON.message);
                		$(location).attr('href', '/documentaryweb/connect-to-app');
                		localStorage.clear();
                	}
                }
            });
        }, error: function (response) {
        	if(response.status == 401){
        		alert(response.responseJSON.message);
        		$(location).attr('href', '/documentaryweb/connect-to-app');
        		localStorage.clear();
        	}
        }
    });
}
function initLoad(roleInfo, result){
	
	var data;
	$.each(result.data, function(index, value) {
		/*firstname, lastname, username, email*/
		data += '<tr><td><input id="fname' + index + '" type="text" disabled="disabled" value="' + value.firstname + '"/></td>';
		data += '<input type="hidden" id ="key-' + index + '"  value ="' + value.id + '"/>';
		data += '<td><input id="lname' + index + '" type="text" disabled="disabled" value="' + value.lastname + '"/></td>';
		data += '<td><input id="uname' + index + '" type="text" disabled="disabled" value="' + value.username + '"/></td>';
		data += '<td><input id="email' + index + '" type="email" disabled="disabled" value="' + value.email + '"/></td>';
		
		/*Role*/
		data += '<td><select id="role-' + index + '" disabled="disabled" class="selectpicker" data-live-search="true">';
		data += '<option value="' + value.role.split("->")[0] + '" selected>' + value.role.split("->")[1] + '</option>';
		var valArr = "";
		$.each(roleInfo.data, function(idx,val){
			if(!(val.name === value.role.split("->")[1]) && valArr.search(val.name)<0){
				data += '<option value="' + val.roleCode + '">' + val.name + '</option>';
				valArr = valArr.concat(val.name);
			}
		});
		
		data += '</select></td>';
		/*Screen Access*/
		data += '<td><input id="screen' + index + '" type="text" data="' + index + '" data-role="tagsinput" disabled="disabled" value="' + (value.role.split("->")[2]).replace(new RegExp('#'.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), 'g'), ',') + '"/></td>';
		
		/*Is Active*/
		data += '<td><div class="custom-control custom-switch">'
			  + '<input type="checkbox" class="custom-control-input" disabled="disabled" id="customSwitchesval' + index + '"';
		
		if(value.isActive==="1"){data += ' checked';}
		
		data += '>'
			  + '<label class="custom-control-label" for="customSwitchesval' + index + '">On</label>'
			  + '</div></td>';
	
		/*Action*/
		data += '<td style="vertical-align:middle">'
			  
			  /*Save*/
			  + '<button id="save-' + index + '" class="btn btn-sm btn-success" style="display:none">'
			  	+ '<span class="fa fa-save"></span>'
			  + '</button>'
			  
			  /*Edit*/
			  + '<button id="edit-' + index + '" class="btn btn-sm btn-info mt-1">'
				+ '<span class="fa fa-edit"></span>'
			  + '</button>'
			  
			  /*Delete*/
			  + '<button id="delete-' + index + '" class="btn btn-sm btn-danger mt-1">'
			  	+ '<span class="fa fa-trash"></span>'
			  + '</button>';
	
		data += '</td></tr>';
	});
	return data;
}

function loadComplete(dataTableFlag){
	var editable = 1;
	
	if ( dataTableFlag ) {

		$('table').dataTable({
			"sScrollX" : "100%",
			"bScrollCollapse" : true,
			"bJQueryUI" : true,
			"bPaginate": true,
			"bInfo": true,
			"bAutoWidth": true,
	        "bProcessing": true,
			"aoColumnDefs" : [ 
				{"aTargets" : [ 0 ], "bSortable" : true}, 
				{"aTargets" : [ '_all' ], "bSortable" : false}
			],
			"aaSorting" : [ [ 0, 'asc' ] ]
		});
		
		setTimeout(function() {
			$(".selectpicker").selectpicker('refresh');
			$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
			$('table').DataTable().columns.adjust();
		}, 5);
		
	}else{
		$('table').dataTable({
			"sScrollX" : "100%",
			"bScrollCollapse" : true,
			"bJQueryUI" : true,
			"bPaginate": true,
			"bInfo": true,
			"bAutoWidth": true,
	        "bProcessing": true,
			"aoColumnDefs" : [ 
				{"aTargets" : [ 0 ], "bSortable" : true}, 
				{"aTargets" : [ '_all' ], "bSortable" : false}
			],
			"aaSorting" : [ [ 0, 'asc' ] ]
		});
	
		
		setTimeout(function() {
			$('table').DataTable().columns.adjust().draw();
		}, 10);
		$('table').on("change", "input[text]", function(){
			$('table').DataTable().columns.adjust().draw();
		});
		$('table').on("click", "button[id^=save-]", function(data, handler){
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			var formData = {
				id : $('#key-' + id)[0].value,
				firstname : $('#fname' + id)[0].value,
				lastname: $('#lname' + id)[0].value,
				username: $('#uname' + id)[0].value,
				email: $('#email' + id)[0].value,
				role: $('#role-' + id)[0].value,
				isActive: $('#customSwitchesval' + id)[0].checked
			};
			jQuery.ajax({
	            type: "PUT",
	            url: "/documentaryrest/rest/usrmgmt/",
	            contentType: "application/json;charset=UTF-8",
	            dataType: "json",
	            data:JSON.stringify(formData),
	            
	            success: function (result) {
	                //console.log(result);                    
	            	$('button[id^=edit-' + id + ']').click();
	            }, error: function(response) {
	                console.log(response);
	            }
	        });
		});
		
		$("table").on("chnage", "select[id^=role-]", function(data, handler){
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			var selectedRoleScreen = getScreenInfo().data.filter(dt => dt.name == data.currentTarget.selectedOptions[0].text).map(m => m.screen).join(',');
			$("input[id^=screen" + id + "]").tagsinput('removeAll');
			$("input[id^=screen" + id + "]").tagsinput('add',selectedRoleScreen);
		});
		
		
		$("table").on("click", "button[id^=edit-]", function(data, handler){
			var id = data.currentTarget.getAttribute('id').split("-")[1];
			editable++;		
			if(editable%2==0){
				$('#fname' + id).removeAttr("disabled");
				$('#lname' + id).removeAttr("disabled");
				$('#uname' + id).removeAttr("disabled");
				$('#email' + id).removeAttr("disabled");
				$('#role-' + id).removeAttr("disabled");
				$('button[data-id=role-' + id + ']').removeClass("disabled");
				$('#customSwitchesval' + id).removeAttr("disabled");
				$('#save-' + id).attr("style","display:block");
			}else{
				$('#fname' + id).attr("disabled","disabled");
				$('#lname' + id).attr("disabled","disabled");
				$('#uname' + id).attr("disabled","disabled");
				$('#email' + id).attr("disabled","'disabled'");
				$('#role-' + id).attr("disabled","'disabled'");
				$('button[data-id=role-' + id + ']').addClass("disabled");
				$('#customSwitchesval' + id).attr("disabled","'disabled'");
				$('#save-' + id).attr("style","display:none");
			}
			
		});
		
		$("table").on("click", "button[id^=delete-]", function(data, handler){
			if (confirm("Are you sure?")) {
				var id = data.currentTarget.getAttribute('id').split("-")[1];
				jQuery.ajax({
		            type: "DELETE",
		            url: "/documentaryrest/rest/usrmgmt/" + $('#key-' + id)[0].value,
		            contentType: "application/json;charset=UTF-8",
		            dataType: "json",
		            success: function (result) {
		                //console.log(result);
		            	$('table').DataTable().row(':eq(' + id + ')').remove().draw(false);
		            }, error: function(response) {
		                console.log(response);
		            }
		        });
			}
		});
		
		/*User Modal*/
		var valArr = "";
		$.each(getScreenInfo().data, function(idx,val){
			if(valArr.search(val.name)<0){
				$('#roleId').append('<option value="' + val.roleCode + '">' + val.name + '</option>');
				valArr = valArr.concat(val.name,',');
			}
		});
	
		$("#screenId").tagsinput('removeAll');
		$("#screenId").tagsinput('add',getScreenInfo().data.filter(dt => dt.name == valArr.split(',')[0]).map(m => m.screen).join(','));
		
		$('#roleId').change(function(data, handler){
			var selectedRoleScreen = getScreenInfo().data.filter(dt => dt.name == data.currentTarget.selectedOptions[0].text).map(m => m.screen).join(',');
			$("#screenId").tagsinput('removeAll');
			$("#screenId").tagsinput('add',selectedRoleScreen);
		});
		
		
		
		$('#userForm').submit(function(e) {
			/*Prevent default form submit*/
			e.preventDefault();
			
			/*Form to JSON*/
			var formData = {};
			$(this).serializeArray().forEach( s => { formData[s.name] = s.value; } );
			
			/*Save Record*/
			jQuery.ajax({
				type: "POST",
				url: "/documentaryrest/rest/usrmgmt/",
				contentType: "application/json;charset=UTF-8",
				dataType: "json",
				data:JSON.stringify(formData),
				success: function (result) {
					$('#userForm').trigger("reset");
					$('#closeUserModel').trigger("click");
					
					init();
				}, error: function(response) {
					console.log(response);
					alert("failed!");
				}
			});
			
		});
		
		$('table').on( 'page.dt', function () {
			setTimeout(function() {
				$(".selectpicker").selectpicker('refresh');
				$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
				$('table').DataTable().columns.adjust();
			}, 5);
		});
		 
		/*Initialize selector*/
		$(".selectpicker").selectpicker('refresh');
		
		/*Initialize tagsInput*/
		$("input[data-role=tagsinput], select[multiple][data-role=tagsinput]").tagsinput();
	}
	
}